-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2018 at 08:38 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kandula`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Index', 'fa-bar-chart', '/', NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', NULL, NULL, '2018-02-21 01:44:28'),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL),
(8, 0, 8, 'System', 'fa-toggle-on', NULL, '2018-01-31 06:40:03', '2018-02-05 02:47:23'),
(9, 15, 10, 'Currency', 'fa-dollar', 'auth/currency', '2018-01-31 06:41:25', '2018-02-05 02:47:23'),
(10, 15, 11, 'Price List', 'fa-list', 'auth/pricelists', '2018-01-31 06:42:34', '2018-02-05 02:47:23'),
(11, 15, 12, 'Tax', 'fa-percent', 'auth/tax', '2018-01-31 06:43:54', '2018-02-05 02:47:23'),
(12, 15, 13, 'Payment Terms', 'fa-align-justify', 'auth/paymentterms', '2018-01-31 06:44:55', '2018-02-05 02:47:23'),
(13, 15, 14, 'Payment Methods', 'fa-bars', 'auth/paymentmethods', '2018-01-31 06:45:25', '2018-02-05 02:47:23'),
(14, 15, 15, 'Stock Adj. Reasons', 'fa-bars', 'auth/stockadjustmentreasons', '2018-01-31 06:45:54', '2018-02-05 02:47:23'),
(15, 8, 9, 'Configuration', 'fa-bars', NULL, '2018-02-05 02:46:47', '2018-02-05 02:47:23'),
(16, 15, 0, 'Customer', 'fa-home', 'auth/company', '2018-02-05 02:51:57', '2018-02-21 02:06:20'),
(17, 15, 0, 'Warehouse', 'fa-stop-circle', 'auth/warehouse', '2018-02-05 02:53:13', '2018-02-05 02:53:13'),
(18, 15, 0, 'Suppliers', 'fa-bars', 'auth/suppliers', '2018-02-05 02:53:52', '2018-02-05 02:53:52'),
(19, 8, 0, 'Product Configuration', 'fa-bars', NULL, '2018-02-05 02:54:29', '2018-02-05 04:36:39'),
(20, 19, 0, 'Product Types', 'fa-bars', 'auth/producttypes', '2018-02-05 02:55:05', '2018-02-05 02:55:05'),
(21, 19, 0, 'Brands', 'fa-bars', 'auth/brands', '2018-02-05 02:55:22', '2018-02-05 02:55:22'),
(22, 19, 0, 'Category', 'fa-bars', 'auth/category', '2018-02-05 02:55:42', '2018-02-05 02:55:42'),
(23, 19, 0, 'Products', 'fa-bars', 'auth/products', '2018-02-05 04:36:19', '2018-02-05 04:37:10'),
(24, 0, 0, 'Inventory', 'fa-cubes', NULL, '2018-02-20 02:00:34', '2018-02-20 02:00:34'),
(25, 24, 0, 'Manage GRN', 'fa-bars', 'auth/grn', '2018-02-20 02:01:48', '2018-02-20 02:01:48'),
(26, 24, 0, 'Stock Transfer', 'fa-bars', 'auth/stock_transfer', '2018-02-20 05:49:20', '2018-02-20 05:49:20'),
(27, 0, 0, 'Reports', 'fa-bars', NULL, '2018-02-20 05:53:29', '2018-02-20 05:53:29'),
(28, 27, 0, 'Bin Card', 'fa-bars', 'auth/bincard', '2018-02-20 05:54:35', '2018-02-20 05:54:35'),
(29, 27, 0, 'Stock', 'fa-bars', 'auth/stock', '2018-02-20 05:55:21', '2018-02-20 05:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:14:01', '2018-01-31 05:14:01'),
(2, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:14:05', '2018-01-31 05:14:05'),
(3, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:14:10', '2018-01-31 05:14:10'),
(4, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{"username":"admin","first_name":"iNat","last_name":"Administrator","email":"arangaw@gmail.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["1",null],"permissions":[null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-01-31 05:14:32', '2018-01-31 05:14:32'),
(5, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-01-31 05:14:32', '2018-01-31 05:14:32'),
(6, 1, 'admin/auth/currencies', 'GET', '127.0.0.1', '[]', '2018-01-31 05:14:42', '2018-01-31 05:14:42'),
(7, 1, 'admin/auth/currencies/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:14:44', '2018-01-31 05:14:44'),
(8, 1, 'admin/auth/currencies', 'POST', '127.0.0.1', '{"name":"Srilankan Rupee","code":"LKR","symbol":"Rs.","rate":"1","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/currencies"}', '2018-01-31 05:15:00', '2018-01-31 05:15:00'),
(9, 1, 'admin/auth/currencies', 'GET', '127.0.0.1', '[]', '2018-01-31 05:15:00', '2018-01-31 05:15:00'),
(10, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:15:08', '2018-01-31 05:15:08'),
(11, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:15:36', '2018-01-31 05:15:36'),
(12, 1, 'admin/auth/pricelists/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:15:40', '2018-01-31 05:15:40'),
(13, 1, 'admin/auth/pricelists', 'POST', '127.0.0.1', '{"name":"Default Price List","code":"DPL","symbol":"1","rate":"1.00","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/pricelists"}', '2018-01-31 05:16:01', '2018-01-31 05:16:01'),
(14, 1, 'admin/auth/pricelists/create', 'GET', '127.0.0.1', '[]', '2018-01-31 05:16:01', '2018-01-31 05:16:01'),
(15, 1, 'admin/auth/pricelists', 'POST', '127.0.0.1', '{"name":"Default Price List","code":"DPL","symbol":"1","rate":"1.00","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 05:17:05', '2018-01-31 05:17:05'),
(16, 1, 'admin/auth/pricelists/create', 'GET', '127.0.0.1', '[]', '2018-01-31 05:17:05', '2018-01-31 05:17:05'),
(17, 1, 'admin/auth/pricelists', 'POST', '127.0.0.1', '{"name":"Default Price List","code":"DPL","currency_id":"1","rate":"1.00","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 05:17:36', '2018-01-31 05:17:36'),
(18, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:17:37', '2018-01-31 05:17:37'),
(19, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:20:51', '2018-01-31 05:20:51'),
(20, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:22:25', '2018-01-31 05:22:25'),
(21, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 05:22:46', '2018-01-31 05:22:46'),
(22, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:27:00', '2018-01-31 05:27:00'),
(23, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:27:51', '2018-01-31 05:27:51'),
(24, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:28:20', '2018-01-31 05:28:20'),
(25, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:28:40', '2018-01-31 05:28:40'),
(26, 1, 'admin/auth/tax/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:28:43', '2018-01-31 05:28:43'),
(27, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:28:45', '2018-01-31 05:28:45'),
(28, 1, 'admin/auth/tax/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:28:51', '2018-01-31 05:28:51'),
(29, 1, 'admin/auth/tax', 'POST', '127.0.0.1', '{"name":"Vat","code":"VAT","rate":"15","is_default":"off","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/tax"}', '2018-01-31 05:29:20', '2018-01-31 05:29:20'),
(30, 1, 'admin/auth/tax/create', 'GET', '127.0.0.1', '[]', '2018-01-31 05:29:21', '2018-01-31 05:29:21'),
(31, 1, 'admin/auth/tax', 'POST', '127.0.0.1', '{"name":"Vat","code":"VAT","rate":"15","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 05:31:49', '2018-01-31 05:31:49'),
(32, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:31:49', '2018-01-31 05:31:49'),
(33, 1, 'admin/auth/tax/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 05:32:44', '2018-01-31 05:32:44'),
(34, 1, 'admin/auth/tax', 'POST', '127.0.0.1', '{"name":"NBT","code":"NBT","rate":".204","is_default":"on","status":"1","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/tax"}', '2018-01-31 05:33:26', '2018-01-31 05:33:26'),
(35, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 05:33:26', '2018-01-31 05:33:26'),
(36, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:18:03', '2018-01-31 06:18:03'),
(37, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:18:40', '2018-01-31 06:18:40'),
(38, 1, 'admin/auth/paymentmethods/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:18:46', '2018-01-31 06:18:46'),
(39, 1, 'admin/auth/paymentmethods', 'POST', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"off","status":"0","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:19:01', '2018-01-31 06:19:01'),
(40, 1, 'admin/auth/paymentmethods/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:19:01', '2018-01-31 06:19:01'),
(41, 1, 'admin/auth/paymentmethods/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:21:43', '2018-01-31 06:21:43'),
(42, 1, 'admin/auth/paymentmethods', 'POST', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"on","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:21:57', '2018-01-31 06:21:57'),
(43, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:21:57', '2018-01-31 06:21:57'),
(44, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:22:20', '2018-01-31 06:22:20'),
(45, 1, 'admin/auth/paymentmethods/1', 'PUT', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"on","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:22:30', '2018-01-31 06:22:30'),
(46, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:22:30', '2018-01-31 06:22:30'),
(47, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:22:38', '2018-01-31 06:22:38'),
(48, 1, 'admin/auth/paymentmethods/1', 'PUT', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"on","status":"INACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:22:50', '2018-01-31 06:22:50'),
(49, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:22:50', '2018-01-31 06:22:50'),
(50, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:24:40', '2018-01-31 06:24:40'),
(51, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '[]', '2018-01-31 06:25:18', '2018-01-31 06:25:18'),
(52, 1, 'admin/auth/paymentmethods/1', 'PUT', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"No","status":"INACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods\\/create"}', '2018-01-31 06:25:26', '2018-01-31 06:25:26'),
(53, 1, 'admin/auth/paymentmethods/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:25:26', '2018-01-31 06:25:26'),
(54, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:25:35', '2018-01-31 06:25:35'),
(55, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:25:38', '2018-01-31 06:25:38'),
(56, 1, 'admin/auth/paymentmethods/1', 'PUT', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"Yes","status":"INACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:25:43', '2018-01-31 06:25:43'),
(57, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:25:44', '2018-01-31 06:25:44'),
(58, 1, 'admin/auth/paymentmethods/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:25:46', '2018-01-31 06:25:46'),
(59, 1, 'admin/auth/paymentmethods/1', 'PUT', '127.0.0.1', '{"name":"Cash on Delivery","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentmethods"}', '2018-01-31 06:25:52', '2018-01-31 06:25:52'),
(60, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:25:52', '2018-01-31 06:25:52'),
(61, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:15', '2018-01-31 06:27:15'),
(62, 1, 'admin/auth/tax/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:27:19', '2018-01-31 06:27:19'),
(63, 1, 'admin/auth/tax/1', 'PUT', '127.0.0.1', '{"name":"Vat","code":"VAT","rate":"15","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/tax"}', '2018-01-31 06:27:23', '2018-01-31 06:27:23'),
(64, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:23', '2018-01-31 06:27:23'),
(65, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:31', '2018-01-31 06:27:31'),
(66, 1, 'admin/auth/pricelists/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:27:37', '2018-01-31 06:27:37'),
(67, 1, 'admin/auth/pricelists/1', 'PUT', '127.0.0.1', '{"name":"Default Price List","code":"DPL","currency_id":"1","rate":"1","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/pricelists"}', '2018-01-31 06:27:40', '2018-01-31 06:27:40'),
(68, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:40', '2018-01-31 06:27:40'),
(69, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:53', '2018-01-31 06:27:53'),
(70, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '[]', '2018-01-31 06:27:58', '2018-01-31 06:27:58'),
(71, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:28:01', '2018-01-31 06:28:01'),
(72, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:28:58', '2018-01-31 06:28:58'),
(73, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:28:59', '2018-01-31 06:28:59'),
(74, 1, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '[]', '2018-01-31 06:29:18', '2018-01-31 06:29:18'),
(75, 1, 'admin/auth/stockadjustmentreasons/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:29:20', '2018-01-31 06:29:20'),
(76, 1, 'admin/auth/stockadjustmentreasons', 'POST', '127.0.0.1', '{"reasons":"Reason 1","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stockadjustmentreasons"}', '2018-01-31 06:29:37', '2018-01-31 06:29:37'),
(77, 1, 'admin/auth/stockadjustmentreasons/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:29:38', '2018-01-31 06:29:38'),
(78, 1, 'admin/auth/stockadjustmentreasons', 'POST', '127.0.0.1', '{"reasons":"Reason 1","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:30:11', '2018-01-31 06:30:11'),
(79, 1, 'admin/auth/stockadjustmentreasons/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:30:11', '2018-01-31 06:30:11'),
(80, 1, 'admin/auth/stockadjustmentreasons', 'POST', '127.0.0.1', '{"reason":"Reason 1","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:30:40', '2018-01-31 06:30:40'),
(81, 1, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '[]', '2018-01-31 06:30:40', '2018-01-31 06:30:40'),
(82, 1, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '[]', '2018-01-31 06:30:59', '2018-01-31 06:30:59'),
(83, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '[]', '2018-01-31 06:31:07', '2018-01-31 06:31:07'),
(84, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '[]', '2018-01-31 06:31:13', '2018-01-31 06:31:13'),
(85, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:31:28', '2018-01-31 06:31:28'),
(86, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:33:01', '2018-01-31 06:33:01'),
(87, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '[]', '2018-01-31 06:34:20', '2018-01-31 06:34:20'),
(88, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:34:21', '2018-01-31 06:34:21'),
(89, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:36:24', '2018-01-31 06:36:24'),
(90, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:36:55', '2018-01-31 06:36:55'),
(91, 1, 'admin/auth/paymentterms', 'POST', '127.0.0.1', '{"name":null,"due_in_days":"-7","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:37:10', '2018-01-31 06:37:10'),
(92, 1, 'admin/auth/paymentterms/create', 'GET', '127.0.0.1', '[]', '2018-01-31 06:37:10', '2018-01-31 06:37:10'),
(93, 1, 'admin/auth/paymentterms', 'POST', '127.0.0.1', '{"name":"Cheque","due_in_days":"90","is_default":"Yes","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:37:28', '2018-01-31 06:37:28'),
(94, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '[]', '2018-01-31 06:37:28', '2018-01-31 06:37:28'),
(95, 1, 'admin/auth/paymentterms/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:37:32', '2018-01-31 06:37:32'),
(96, 1, 'admin/auth/paymentterms/1/edit', 'GET', '127.0.0.1', '[]', '2018-01-31 06:37:48', '2018-01-31 06:37:48'),
(97, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:37:52', '2018-01-31 06:37:52'),
(98, 1, 'admin/auth/paymentterms/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:37:55', '2018-01-31 06:37:55'),
(99, 1, 'admin/auth/paymentterms/1', 'PUT', '127.0.0.1', '{"name":"Cheque","due_in_days":"90","is_default":"No","status":"ACTIVE","_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/paymentterms"}', '2018-01-31 06:37:58', '2018-01-31 06:37:58'),
(100, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '[]', '2018-01-31 06:37:59', '2018-01-31 06:37:59'),
(101, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:38:46', '2018-01-31 06:38:46'),
(102, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"0","title":"System","icon":"fa-toggle-on","uri":null,"roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:40:03', '2018-01-31 06:40:03'),
(103, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:40:03', '2018-01-31 06:40:03'),
(104, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Currency","icon":"fa-dollar","uri":"auth\\/currency","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:41:25', '2018-01-31 06:41:25'),
(105, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:41:25', '2018-01-31 06:41:25'),
(106, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Price List","icon":"fa-list","uri":"auth\\/pricelists","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:42:34', '2018-01-31 06:42:34'),
(107, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:42:34', '2018-01-31 06:42:34'),
(108, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Tax","icon":"fa-percent","uri":"auth\\/tax","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:43:54', '2018-01-31 06:43:54'),
(109, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:43:54', '2018-01-31 06:43:54'),
(110, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Payment Terms","icon":"fa-align-justify","uri":"auth\\/paymentterms","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:44:55', '2018-01-31 06:44:55'),
(111, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:44:55', '2018-01-31 06:44:55'),
(112, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Payment Methods","icon":"fa-bars","uri":"auth\\/paymentmethods","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:45:25', '2018-01-31 06:45:25'),
(113, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:45:25', '2018-01-31 06:45:25'),
(114, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Stock Adjustment Reasons","icon":"fa-bars","uri":"auth\\/stockadjustmentreasons","roles":["1",null],"_token":"GvI3D8jP5JycbxISkZ7lSY4Rm4s6Fdsst4CYJn6P"}', '2018-01-31 06:45:54', '2018-01-31 06:45:54'),
(115, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:45:54', '2018-01-31 06:45:54'),
(116, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-01-31 06:45:56', '2018-01-31 06:45:56'),
(117, 1, 'admin/auth/currency', 'GET', '127.0.0.1', '[]', '2018-01-31 06:46:31', '2018-01-31 06:46:31'),
(118, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:46:34', '2018-01-31 06:46:34'),
(119, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:46:37', '2018-01-31 06:46:37'),
(120, 1, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:46:39', '2018-01-31 06:46:39'),
(121, 1, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:46:41', '2018-01-31 06:46:41'),
(122, 1, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-01-31 06:46:43', '2018-01-31 06:46:43'),
(123, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-02 03:37:48', '2018-02-02 03:37:48'),
(124, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 04:05:08', '2018-02-02 04:05:08'),
(125, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:05:15', '2018-02-02 04:05:15'),
(126, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"tax_number":null,"phone_number":null,"fax_number":null,"website":null,"email_address":null,"description":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"status":"ACTIVE","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company"}', '2018-02-02 04:07:17', '2018-02-02 04:07:17'),
(127, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:07:17', '2018-02-02 04:07:17'),
(128, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:08:07', '2018-02-02 04:08:07'),
(129, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:15:55', '2018-02-02 04:15:55'),
(130, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:16:10', '2018-02-02 04:16:10'),
(131, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:16:10', '2018-02-02 04:16:10'),
(132, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:16:43', '2018-02-02 04:16:43'),
(133, 1, 'admin/auth/tax/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:16:45', '2018-02-02 04:16:45'),
(134, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:17:33', '2018-02-02 04:17:33'),
(135, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 04:17:41', '2018-02-02 04:17:41'),
(136, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:17:44', '2018-02-02 04:17:44'),
(137, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company"}', '2018-02-02 04:17:58', '2018-02-02 04:17:58'),
(138, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:17:59', '2018-02-02 04:17:59'),
(139, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 1","company_code":"CC1","logo":null,"description":"Description","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:19:19', '2018-02-02 04:19:19'),
(140, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:19:19', '2018-02-02 04:19:19'),
(141, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 1","company_code":"CC1","logo":null,"description":"Description","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183","fax_number":"0112456741","website":"http:\\/\\/www.google.com","email_address":"arangaw@gmail.com","default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:20:33', '2018-02-02 04:20:33'),
(142, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:20:33', '2018-02-02 04:20:33'),
(143, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 1","company_code":"CC1","logo":null,"description":"Description","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183","fax_number":"0112456741","website":"http:\\/\\/www.google.com","email_address":"arangaw@gmail.com","default_price_list_id":"1","default_tax_type_id":"1","default_payment_term_id":"1","default_payment_method_id":"1","discount_rate":"0.25","minimum_order_value":"100000.00","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:21:02', '2018-02-02 04:21:02'),
(144, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 04:21:02', '2018-02-02 04:21:02'),
(145, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:21:07', '2018-02-02 04:21:07'),
(146, 1, 'admin/auth/company/1', 'PUT', '127.0.0.1', '{"company_name":"Company 1","company_code":"CC1","logo":null,"description":"Description","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183","fax_number":"0112456741","website":null,"email_address":"arangaw@gmail.com","default_price_list_id":"1","default_tax_type_id":"1","default_payment_term_id":"1","default_payment_method_id":"1","discount_rate":"0.25","minimum_order_value":"100000.00","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company"}', '2018-02-02 04:21:28', '2018-02-02 04:21:28'),
(147, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 04:21:28', '2018-02-02 04:21:28'),
(148, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 04:21:59', '2018-02-02 04:21:59'),
(149, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:22:04', '2018-02-02 04:22:04'),
(150, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:22:11', '2018-02-02 04:22:11'),
(151, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:22:14', '2018-02-02 04:22:14'),
(152, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company"}', '2018-02-02 04:22:18', '2018-02-02 04:22:18'),
(153, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:22:18', '2018-02-02 04:22:18'),
(154, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:26:23', '2018-02-02 04:26:23'),
(155, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:26:23', '2018-02-02 04:26:23'),
(156, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:35:35', '2018-02-02 04:35:35'),
(157, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"logo":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company\\/1\\/edit"}', '2018-02-02 04:35:37', '2018-02-02 04:35:37'),
(158, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:35:37', '2018-02-02 04:35:37'),
(159, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:45:48', '2018-02-02 04:45:48'),
(160, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:46:25', '2018-02-02 04:46:25'),
(161, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:46:55', '2018-02-02 04:46:55'),
(162, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:47:21', '2018-02-02 04:47:21'),
(163, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:47:43', '2018-02-02 04:47:43'),
(164, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":null,"company_code":null,"description":null,"address_line1":null,"address_line2":null,"suburb":null,"city":null,"post_code":null,"tax_number":null,"status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:47:46', '2018-02-02 04:47:46'),
(165, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:47:46', '2018-02-02 04:47:46'),
(166, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 2","company_code":"CC2","description":"Descrption 2","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":null,"fax_number":null,"website":null,"email_address":null,"default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:48:44', '2018-02-02 04:48:44'),
(167, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:48:44', '2018-02-02 04:48:44'),
(168, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 2","company_code":"CC2","description":"Descrption 2","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183_","fax_number":"0718068183_","website":"http:\\/\\/www.google.com","email_address":"arangaw@gmail.com","default_price_list_id":null,"default_tax_type_id":null,"default_payment_term_id":null,"default_payment_method_id":null,"discount_rate":null,"minimum_order_value":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:49:18', '2018-02-02 04:49:18'),
(169, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '[]', '2018-02-02 04:49:18', '2018-02-02 04:49:18'),
(170, 1, 'admin/auth/company', 'POST', '127.0.0.1', '{"company_name":"Company 2","company_code":"CC2","description":"Descrption 2","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183_","fax_number":"0718068183_","website":"http:\\/\\/www.google.com","email_address":"arangaw@gmail.com","default_price_list_id":"1","default_tax_type_id":"1","default_payment_term_id":"1","default_payment_method_id":"1","discount_rate":"0.25","minimum_order_value":"25000.00","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 04:49:43', '2018-02-02 04:49:43'),
(171, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 04:49:43', '2018-02-02 04:49:43'),
(172, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 04:49:48', '2018-02-02 04:49:48'),
(173, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:01:24', '2018-02-02 05:01:24'),
(174, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:26', '2018-02-02 05:01:26'),
(175, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:29', '2018-02-02 05:01:29'),
(176, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:31', '2018-02-02 05:01:31'),
(177, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:33', '2018-02-02 05:01:33'),
(178, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:37', '2018-02-02 05:01:37'),
(179, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:53', '2018-02-02 05:01:53'),
(180, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:01:57', '2018-02-02 05:01:57'),
(181, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{"username":"admin","first_name":"iNat","last_name":"Administrator","email":"arangaw@gmail.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["1",null],"permissions":[null],"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-02-02 05:02:12', '2018-02-02 05:02:12'),
(182, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:02:13', '2018-02-02 05:02:13'),
(183, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{"username":"admin","first_name":"iNat","last_name":"Administrator","email":"arangaw@gmail.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["1",null],"permissions":[null],"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT"}', '2018-02-02 05:02:41', '2018-02-02 05:02:41'),
(184, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:02:41', '2018-02-02 05:02:41'),
(185, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:05', '2018-02-02 05:03:05'),
(186, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:06', '2018-02-02 05:03:06'),
(187, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:06', '2018-02-02 05:03:06'),
(188, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:07', '2018-02-02 05:03:07'),
(189, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:07', '2018-02-02 05:03:07'),
(190, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:07', '2018-02-02 05:03:07'),
(191, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:07', '2018-02-02 05:03:07'),
(192, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:08', '2018-02-02 05:03:08'),
(193, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:08', '2018-02-02 05:03:08'),
(194, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:39', '2018-02-02 05:03:39'),
(195, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{"username":"admin","first_name":"iNat","last_name":"Administrator","email":"arangaw@gmail.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["1",null],"permissions":[null],"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT"}', '2018-02-02 05:03:49', '2018-02-02 05:03:49'),
(196, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:03:50', '2018-02-02 05:03:50'),
(197, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 05:04:31', '2018-02-02 05:04:31'),
(198, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:04:34', '2018-02-02 05:04:34'),
(199, 1, 'admin/auth/company/2', 'PUT', '127.0.0.1', '{"company_name":"Company 2","company_code":"CC2","description":"Descrption 2","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183_","fax_number":"0718068183_","website":"http:\\/\\/www.google.com","email_address":"arangaw@gmail.com","default_price_list_id":"1","default_tax_type_id":"1","default_payment_term_id":"1","default_payment_method_id":"1","discount_rate":"0.25","minimum_order_value":"25000.00","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/company"}', '2018-02-02 05:04:43', '2018-02-02 05:04:43'),
(200, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 05:04:43', '2018-02-02 05:04:43'),
(201, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:04:48', '2018-02-02 05:04:48'),
(202, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:05:28', '2018-02-02 05:05:28'),
(203, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:07:32', '2018-02-02 05:07:32'),
(204, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:08:33', '2018-02-02 05:08:33'),
(205, 1, 'admin/auth/company/2', 'PUT', '127.0.0.1', '{"company_name":"Company 2","company_code":"CC2","description":"Descrption 2","address_line1":"Address 1","address_line2":"Address 2","suburb":"Suburb","city":"City","post_code":"12000","tax_number":"1-1245-A123456","status":"ACTIVE","phone_number":"0718068183_","fax_number":"0718068183_","website":null,"email_address":"arangaw@gmail.com","default_price_list_id":"1","default_tax_type_id":"1","default_payment_term_id":"1","default_payment_method_id":"1","discount_rate":"0.25","minimum_order_value":"25000.00","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_method":"PUT"}', '2018-02-02 05:08:53', '2018-02-02 05:08:53'),
(206, 1, 'admin/auth/company/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-02 05:08:53', '2018-02-02 05:08:53'),
(207, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:11:32', '2018-02-02 05:11:32'),
(208, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:11:34', '2018-02-02 05:11:34'),
(209, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 05:11:34', '2018-02-02 05:11:34'),
(210, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 05:24:34', '2018-02-02 05:24:34'),
(211, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-02 05:24:40', '2018-02-02 05:24:40'),
(212, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-02 05:25:24', '2018-02-02 05:25:24'),
(213, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:25:27', '2018-02-02 05:25:27'),
(214, 1, 'admin/auth/warehouse', 'POST', '127.0.0.1', '{"name":null,"address":null,"city":null,"post_code":null,"phone_number":null,"email_address":null,"contact_person":null,"_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/warehouse"}', '2018-02-02 05:25:31', '2018-02-02 05:25:31'),
(215, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '[]', '2018-02-02 05:25:31', '2018-02-02 05:25:31'),
(216, 1, 'admin/auth/warehouse', 'POST', '127.0.0.1', '{"name":"Warehouse 1","address":"Address","city":"City","post_code":"12000","phone_number":"0718068183_","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 05:26:32', '2018-02-02 05:26:32'),
(217, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '[]', '2018-02-02 05:26:33', '2018-02-02 05:26:33'),
(218, 1, 'admin/auth/warehouse', 'POST', '127.0.0.1', '{"name":"Warehouse 1","address":"Address","city":"City","post_code":"12000","phone_number":"0718068183_","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 05:29:45', '2018-02-02 05:29:45'),
(219, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '[]', '2018-02-02 05:29:45', '2018-02-02 05:29:45'),
(220, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '[]', '2018-02-02 05:29:49', '2018-02-02 05:29:49'),
(221, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '[]', '2018-02-02 05:31:49', '2018-02-02 05:31:49'),
(222, 1, 'admin/auth/warehouse', 'POST', '127.0.0.1', '{"name":"Warehouse 1","address":"\\"Kuladeepa\\",Thalgamuwa, Devanagala","city":"City","post_code":"12000","phone_number":"0718068183_","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","status":"ACTIVE","_token":"Wf1EpkLlT68DC1IL0WQtXi2lwhsX7pqIg1LmLsAZ"}', '2018-02-02 05:32:17', '2018-02-02 05:32:17'),
(223, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-02 05:32:18', '2018-02-02 05:32:18'),
(224, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"id":null,"phone_number":"124","email_address":null,"post_code":null,"_pjax":"#pjax-container"}', '2018-02-02 05:32:29', '2018-02-02 05:32:29'),
(225, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:32:33', '2018-02-02 05:32:33'),
(226, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container","id":null,"phone_number":"806","email_address":null,"post_code":null}', '2018-02-02 05:32:43', '2018-02-02 05:32:43'),
(227, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-02 05:34:38', '2018-02-02 05:34:38'),
(228, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"id":null,"status":null,"company_name":null,"phone_number":"806","_pjax":"#pjax-container"}', '2018-02-02 05:34:47', '2018-02-02 05:34:47'),
(229, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:35:21', '2018-02-02 05:35:21'),
(230, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"id":null,"status":null,"company_name":null,"phone_number":"806"}', '2018-02-02 05:35:21', '2018-02-02 05:35:21'),
(231, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"id":null,"status":null,"company_name":null,"phone_number":"806"}', '2018-02-02 05:35:45', '2018-02-02 05:35:45'),
(232, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:35:51', '2018-02-02 05:35:51'),
(233, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 05:36:03', '2018-02-02 05:36:03'),
(234, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-02 07:32:46', '2018-02-02 07:32:46'),
(235, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2018-02-02 23:27:16', '2018-02-02 23:27:16'),
(236, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '[]', '2018-02-03 01:13:03', '2018-02-03 01:13:03'),
(237, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '[]', '2018-02-03 01:13:24', '2018-02-03 01:13:24'),
(238, 1, 'admin/auth/suppliers/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 01:13:29', '2018-02-03 01:13:29'),
(239, 1, 'admin/auth/suppliers', 'POST', '127.0.0.1', '{"name":"Supplier","address":"Address","city":"City","post_code":"12000","phone_number":"0718068183","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/suppliers"}', '2018-02-03 01:21:05', '2018-02-03 01:21:05'),
(240, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '[]', '2018-02-03 01:21:05', '2018-02-03 01:21:05'),
(241, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '{"id":null,"name":"A","_pjax":"#pjax-container"}', '2018-02-03 01:21:17', '2018-02-03 01:21:17'),
(242, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-03 02:02:22', '2018-02-03 02:02:22'),
(243, 1, 'admin/auth/brands', 'GET', '127.0.0.1', '[]', '2018-02-03 02:03:34', '2018-02-03 02:03:34'),
(244, 1, 'admin/auth/brands/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:03:38', '2018-02-03 02:03:38'),
(245, 1, 'admin/auth/brands', 'POST', '127.0.0.1', '{"type":null,"status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/brands"}', '2018-02-03 02:03:48', '2018-02-03 02:03:48'),
(246, 1, 'admin/auth/brands/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:03:48', '2018-02-03 02:03:48'),
(247, 1, 'admin/auth/brands', 'POST', '127.0.0.1', '{"type":"Brand","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY"}', '2018-02-03 02:03:56', '2018-02-03 02:03:56'),
(248, 1, 'admin/auth/brands', 'GET', '127.0.0.1', '[]', '2018-02-03 02:03:56', '2018-02-03 02:03:56'),
(249, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '[]', '2018-02-03 02:04:03', '2018-02-03 02:04:03'),
(250, 1, 'admin/auth/producttypes/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:04:05', '2018-02-03 02:04:05'),
(251, 1, 'admin/auth/producttypes', 'POST', '127.0.0.1', '{"type":null,"status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/producttypes"}', '2018-02-03 02:04:07', '2018-02-03 02:04:07'),
(252, 1, 'admin/auth/producttypes/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:04:08', '2018-02-03 02:04:08'),
(253, 1, 'admin/auth/producttypes', 'POST', '127.0.0.1', '{"type":"Raw Meterial","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY"}', '2018-02-03 02:04:34', '2018-02-03 02:04:34'),
(254, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '[]', '2018-02-03 02:04:35', '2018-02-03 02:04:35'),
(255, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '[]', '2018-02-03 02:05:42', '2018-02-03 02:05:42'),
(256, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:10:52', '2018-02-03 02:10:52'),
(257, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:14:08', '2018-02-03 02:14:08'),
(258, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:14:16', '2018-02-03 02:14:16'),
(259, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:17:12', '2018-02-03 02:17:12'),
(260, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"parent_id":null,"order":"1","title":"Root","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/category"}', '2018-02-03 02:18:44', '2018-02-03 02:18:44'),
(261, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:18:44', '2018-02-03 02:18:44'),
(262, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:19:53', '2018-02-03 02:19:53'),
(263, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"parent_id":null,"order":"0","title":"Root","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY"}', '2018-02-03 02:20:03', '2018-02-03 02:20:03'),
(264, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:20:03', '2018-02-03 02:20:03'),
(265, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:24:46', '2018-02-03 02:24:46'),
(266, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:24:52', '2018-02-03 02:24:52'),
(267, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:24:53', '2018-02-03 02:24:53'),
(268, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:24:59', '2018-02-03 02:24:59'),
(269, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:25:25', '2018-02-03 02:25:25'),
(270, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:27', '2018-02-03 02:25:27'),
(271, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:28', '2018-02-03 02:25:28'),
(272, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:28', '2018-02-03 02:25:28'),
(273, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:28', '2018-02-03 02:25:28'),
(274, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:29', '2018-02-03 02:25:29'),
(275, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:29', '2018-02-03 02:25:29'),
(276, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:29', '2018-02-03 02:25:29'),
(277, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:25:31', '2018-02-03 02:25:31'),
(278, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:32', '2018-02-03 02:25:32'),
(279, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:25:43', '2018-02-03 02:25:43'),
(280, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:43', '2018-02-03 02:25:43'),
(281, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:25:55', '2018-02-03 02:25:55'),
(282, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:25:57', '2018-02-03 02:25:57');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(283, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '[]', '2018-02-03 02:27:01', '2018-02-03 02:27:01'),
(284, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"parent_id":"1","order":"1","title":"Main Category","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/category"}', '2018-02-03 02:27:21', '2018-02-03 02:27:21'),
(285, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:27:21', '2018-02-03 02:27:21'),
(286, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:27:29', '2018-02-03 02:27:29'),
(287, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"parent_id":"2","order":"2","title":"Sub Category MC 1","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/category"}', '2018-02-03 02:27:56', '2018-02-03 02:27:56'),
(288, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:27:57', '2018-02-03 02:27:57'),
(289, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:28:14', '2018-02-03 02:28:14'),
(290, 1, 'admin/auth/category/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:28:22', '2018-02-03 02:28:22'),
(291, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"parent_id":"1","order":"20","title":"Main Category 2","status":"ACTIVE","_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/category"}', '2018-02-03 02:28:45', '2018-02-03 02:28:45'),
(292, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:28:46', '2018-02-03 02:28:46'),
(293, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_order":"[{\\"id\\":1,\\"children\\":[{\\"id\\":4},{\\"id\\":2,\\"children\\":[{\\"id\\":3}]}]}]"}', '2018-02-03 02:29:02', '2018-02-03 02:29:02'),
(294, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:29:03', '2018-02-03 02:29:03'),
(295, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:29:08', '2018-02-03 02:29:08'),
(296, 1, 'admin/auth/category', 'POST', '127.0.0.1', '{"_token":"9T17BCUOGsev06B0duqtpWPXc1Ok2FRLQ6COveyY","_order":"[{\\"id\\":1,\\"children\\":[{\\"id\\":2,\\"children\\":[{\\"id\\":3}]},{\\"id\\":4}]}]"}', '2018-02-03 02:29:50', '2018-02-03 02:29:50'),
(297, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:29:50', '2018-02-03 02:29:50'),
(298, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:31:39', '2018-02-03 02:31:39'),
(299, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:32:56', '2018-02-03 02:32:56'),
(300, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:32:57', '2018-02-03 02:32:57'),
(301, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:32:57', '2018-02-03 02:32:57'),
(302, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:32:58', '2018-02-03 02:32:58'),
(303, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:32:58', '2018-02-03 02:32:58'),
(304, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:00', '2018-02-03 02:33:00'),
(305, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:00', '2018-02-03 02:33:00'),
(306, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:00', '2018-02-03 02:33:00'),
(307, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:01', '2018-02-03 02:33:01'),
(308, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:01', '2018-02-03 02:33:01'),
(309, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:01', '2018-02-03 02:33:01'),
(310, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:01', '2018-02-03 02:33:01'),
(311, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:01', '2018-02-03 02:33:01'),
(312, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:02', '2018-02-03 02:33:02'),
(313, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:03', '2018-02-03 02:33:03'),
(314, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:04', '2018-02-03 02:33:04'),
(315, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:05', '2018-02-03 02:33:05'),
(316, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:33:05', '2018-02-03 02:33:05'),
(317, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:34:08', '2018-02-03 02:34:08'),
(318, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:34:09', '2018-02-03 02:34:09'),
(319, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:34:09', '2018-02-03 02:34:09'),
(320, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:34:33', '2018-02-03 02:34:33'),
(321, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:37:05', '2018-02-03 02:37:05'),
(322, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:37:06', '2018-02-03 02:37:06'),
(323, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:37:07', '2018-02-03 02:37:07'),
(324, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:37:25', '2018-02-03 02:37:25'),
(325, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:39:59', '2018-02-03 02:39:59'),
(326, 1, 'admin/auth/category', 'GET', '127.0.0.1', '[]', '2018-02-03 02:40:17', '2018-02-03 02:40:17'),
(327, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-03 02:42:16', '2018-02-03 02:42:16'),
(328, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-05 02:42:24', '2018-02-05 02:42:24'),
(329, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:45:03', '2018-02-05 02:45:03'),
(330, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:45:20', '2018-02-05 02:45:20'),
(331, 1, 'admin/auth/menu/14', 'PUT', '127.0.0.1', '{"parent_id":"8","title":"Stock Adj. Reasons","icon":"fa-bars","uri":"auth\\/stockadjustmentreasons","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-05 02:45:32', '2018-02-05 02:45:32'),
(332, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:45:32', '2018-02-05 02:45:32'),
(333, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Configuration","icon":"fa-bars","uri":null,"roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:46:47', '2018-02-05 02:46:47'),
(334, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:46:47', '2018-02-05 02:46:47'),
(335, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_order":"[{\\"id\\":1},{\\"id\\":2,\\"children\\":[{\\"id\\":3},{\\"id\\":4},{\\"id\\":5},{\\"id\\":6},{\\"id\\":7}]},{\\"id\\":8,\\"children\\":[{\\"id\\":15,\\"children\\":[{\\"id\\":9},{\\"id\\":10},{\\"id\\":11},{\\"id\\":12},{\\"id\\":13},{\\"id\\":14}]}]}]"}', '2018-02-05 02:47:23', '2018-02-05 02:47:23'),
(336, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:47:24', '2018-02-05 02:47:24'),
(337, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:49:07', '2018-02-05 02:49:07'),
(338, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"15","title":"Company","icon":"fa-home","uri":"auth\\/company","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:51:56', '2018-02-05 02:51:56'),
(339, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:51:57', '2018-02-05 02:51:57'),
(340, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"15","title":"Warehouse","icon":"fa-stop-circle","uri":"auth\\/warehouse","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:53:13', '2018-02-05 02:53:13'),
(341, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:53:13', '2018-02-05 02:53:13'),
(342, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"15","title":"Suppliers","icon":"fa-bars","uri":"auth\\/suppliers","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:53:52', '2018-02-05 02:53:52'),
(343, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:53:52', '2018-02-05 02:53:52'),
(344, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"8","title":"Products","icon":"fa-bars","uri":null,"roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:54:29', '2018-02-05 02:54:29'),
(345, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:54:29', '2018-02-05 02:54:29'),
(346, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"19","title":"Product Types","icon":"fa-bars","uri":"auth\\/producttypes","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:55:05', '2018-02-05 02:55:05'),
(347, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:55:05', '2018-02-05 02:55:05'),
(348, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"19","title":"Brands","icon":"fa-bars","uri":"auth\\/brands","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:55:21', '2018-02-05 02:55:21'),
(349, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:55:22', '2018-02-05 02:55:22'),
(350, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"19","title":"Category","icon":"fa-bars","uri":"auth\\/category","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 02:55:42', '2018-02-05 02:55:42'),
(351, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:55:42', '2018-02-05 02:55:42'),
(352, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 02:55:48', '2018-02-05 02:55:48'),
(353, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:55:53', '2018-02-05 02:55:53'),
(354, 1, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:55:56', '2018-02-05 02:55:56'),
(355, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:55:58', '2018-02-05 02:55:58'),
(356, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:56:00', '2018-02-05 02:56:00'),
(357, 1, 'admin/auth/producttypes/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 02:56:11', '2018-02-05 02:56:11'),
(358, 1, 'admin/auth/producttypes', 'POST', '127.0.0.1', '{"type":"Finished Goods","status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/producttypes"}', '2018-02-05 02:56:35', '2018-02-05 02:56:35'),
(359, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '[]', '2018-02-05 02:56:35', '2018-02-05 02:56:35'),
(360, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:01:48', '2018-02-05 03:01:48'),
(361, 1, 'admin/auth/company/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:01:51', '2018-02-05 03:01:51'),
(362, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:24:49', '2018-02-05 03:24:49'),
(363, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:25:12', '2018-02-05 03:25:12'),
(364, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:25:14', '2018-02-05 03:25:14'),
(365, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:25:15', '2018-02-05 03:25:15'),
(366, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:25:52', '2018-02-05 03:25:52'),
(367, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:25:55', '2018-02-05 03:25:55'),
(368, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":null,"sku":null,"description":null,"product_type_id":null,"supplier_id":null,"brand_id":null,"cost":null,"buying_price":null,"wholesale_price":null,"retail_price":null,"manage_stock_level":"YES","opening_stock":null,"re_order_level":null,"category_id":null,"status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/products"}', '2018-02-05 03:26:42', '2018-02-05 03:26:42'),
(369, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:26:42', '2018-02-05 03:26:42'),
(370, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:39:10', '2018-02-05 03:39:10'),
(371, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:42:54', '2018-02-05 03:42:54'),
(372, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:47:57', '2018-02-05 03:47:57'),
(373, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:48:27', '2018-02-05 03:48:27'),
(374, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:48:59', '2018-02-05 03:48:59'),
(375, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":null,"sku":null,"description":null,"product_type_id":null,"supplier_id":null,"brand_id":null,"cost":null,"buying_price":null,"wholesale_price":null,"retail_price":null,"manage_stock_level":"YES","re_order_level":null,"category_id":"0","status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 03:49:02', '2018-02-05 03:49:02'),
(376, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 03:49:03', '2018-02-05 03:49:03'),
(377, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":"Product A","sku":"123","description":"Description","product_type_id":"2","supplier_id":null,"brand_id":null,"cost":"100","buying_price":"110","wholesale_price":"118","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 03:49:47', '2018-02-05 03:49:47'),
(378, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:49:47', '2018-02-05 03:49:47'),
(379, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:50:46', '2018-02-05 03:50:46'),
(380, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:52:16', '2018-02-05 03:52:16'),
(381, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:52:44', '2018-02-05 03:52:44'),
(382, 1, 'admin/auth/suppliers', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:52:47', '2018-02-05 03:52:47'),
(383, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:52:54', '2018-02-05 03:52:54'),
(384, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 03:53:00', '2018-02-05 03:53:00'),
(385, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:54:28', '2018-02-05 03:54:28'),
(386, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:55:58', '2018-02-05 03:55:58'),
(387, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:55:59', '2018-02-05 03:55:59'),
(388, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 03:59:55', '2018-02-05 03:59:55'),
(389, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:02:33', '2018-02-05 04:02:33'),
(390, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:12:41', '2018-02-05 04:12:41'),
(391, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:17:42', '2018-02-05 04:17:42'),
(392, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:17:59', '2018-02-05 04:17:59'),
(393, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:18:15', '2018-02-05 04:18:15'),
(394, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:21:37', '2018-02-05 04:21:37'),
(395, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:30:40', '2018-02-05 04:30:40'),
(396, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:31:55', '2018-02-05 04:31:55'),
(397, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:32:36', '2018-02-05 04:32:36'),
(398, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:33:15', '2018-02-05 04:33:15'),
(399, 1, 'admin/auth/products/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:34:10', '2018-02-05 04:34:10'),
(400, 1, 'admin/auth/products/1', 'PUT', '127.0.0.1', '{"product_name":"Product A","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","buying_price":"110","wholesale_price":"118","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/products"}', '2018-02-05 04:34:20', '2018-02-05 04:34:20'),
(401, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:34:20', '2018-02-05 04:34:20'),
(402, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:34:24', '2018-02-05 04:34:24'),
(403, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":"Product B","sku":"123","description":null,"product_type_id":"1","supplier_id":null,"brand_id":null,"cost":null,"buying_price":null,"wholesale_price":null,"retail_price":null,"manage_stock_level":"YES","re_order_level":null,"category_id":"0","status":"ACTIVE","_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/products"}', '2018-02-05 04:34:41', '2018-02-05 04:34:41'),
(404, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-05 04:34:42', '2018-02-05 04:34:42'),
(405, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:34:46', '2018-02-05 04:34:46'),
(406, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:34:52', '2018-02-05 04:34:52'),
(407, 1, 'admin/auth/products/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:34:53', '2018-02-05 04:34:53'),
(408, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:35:09', '2018-02-05 04:35:09'),
(409, 1, 'admin/auth/menu/19/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:35:16', '2018-02-05 04:35:16'),
(410, 1, 'admin/auth/menu/19', 'PUT', '127.0.0.1', '{"parent_id":"8","title":"Products","icon":"fa-bars","uri":"auth\\/product","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-05 04:35:25', '2018-02-05 04:35:25'),
(411, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:35:25', '2018-02-05 04:35:25'),
(412, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:35:31', '2018-02-05 04:35:31'),
(413, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"19","title":"Products","icon":"fa-bars","uri":"auth\\/product","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud"}', '2018-02-05 04:36:19', '2018-02-05 04:36:19'),
(414, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:36:19', '2018-02-05 04:36:19'),
(415, 1, 'admin/auth/menu/19/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:36:24', '2018-02-05 04:36:24'),
(416, 1, 'admin/auth/menu/19', 'PUT', '127.0.0.1', '{"parent_id":"8","title":"Product Configuration","icon":"fa-bars","uri":null,"roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-05 04:36:39', '2018-02-05 04:36:39'),
(417, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:36:39', '2018-02-05 04:36:39'),
(418, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:36:42', '2018-02-05 04:36:42'),
(419, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:36:54', '2018-02-05 04:36:54'),
(420, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:36:57', '2018-02-05 04:36:57'),
(421, 1, 'admin/auth/menu/23/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:37:05', '2018-02-05 04:37:05'),
(422, 1, 'admin/auth/menu/23', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Products","icon":"fa-bars","uri":"auth\\/products","roles":["1",null],"_token":"ZPSfpZ3b8PpAZGsJ6wluWUur2VVN1zwxJyhnpgud","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-05 04:37:10', '2018-02-05 04:37:10'),
(423, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:37:10', '2018-02-05 04:37:10'),
(424, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-05 04:37:17', '2018-02-05 04:37:17'),
(425, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:37:24', '2018-02-05 04:37:24'),
(426, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:38:04', '2018-02-05 04:38:04'),
(427, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:38:08', '2018-02-05 04:38:08'),
(428, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:38:53', '2018-02-05 04:38:53'),
(429, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:39:37', '2018-02-05 04:39:37'),
(430, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-05 04:39:49', '2018-02-05 04:39:49'),
(431, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:39:52', '2018-02-05 04:39:52'),
(432, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-05 04:40:01', '2018-02-05 04:40:01'),
(433, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-06 16:26:52', '2018-02-06 16:26:52'),
(434, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:16', '2018-02-06 16:27:16'),
(435, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:19', '2018-02-06 16:27:19'),
(436, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:34', '2018-02-06 16:27:34'),
(437, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:37', '2018-02-06 16:27:37'),
(438, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:45', '2018-02-06 16:27:45'),
(439, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:51', '2018-02-06 16:27:51'),
(440, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:27:54', '2018-02-06 16:27:54'),
(441, 1, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:28:25', '2018-02-06 16:28:25'),
(442, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:28:28', '2018-02-06 16:28:28'),
(443, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:28:33', '2018-02-06 16:28:33'),
(444, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:28:35', '2018-02-06 16:28:35'),
(445, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:28:55', '2018-02-06 16:28:55'),
(446, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:29:01', '2018-02-06 16:29:01'),
(447, 1, 'admin/auth/pricelists/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-06 16:29:03', '2018-02-06 16:29:03'),
(448, 1, 'admin/auth/company', 'GET', '127.0.0.1', '[]', '2018-02-12 06:49:01', '2018-02-12 06:49:01'),
(449, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:19', '2018-02-12 06:49:19'),
(450, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:23', '2018-02-12 06:49:23'),
(451, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:24', '2018-02-12 06:49:24'),
(452, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:25', '2018-02-12 06:49:25'),
(453, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:26', '2018-02-12 06:49:26'),
(454, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-12 06:49:28', '2018-02-12 06:49:28'),
(455, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:41:47', '2018-02-13 04:41:47'),
(456, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:41:55', '2018-02-13 04:41:55'),
(457, 1, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:41:58', '2018-02-13 04:41:58'),
(458, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:42:07', '2018-02-13 04:42:07'),
(459, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:42:23', '2018-02-13 04:42:23'),
(460, 1, 'admin/auth/products/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:42:25', '2018-02-13 04:42:25'),
(461, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:42:49', '2018-02-13 04:42:49'),
(462, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:20', '2018-02-13 04:43:20'),
(463, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:28', '2018-02-13 04:43:28'),
(464, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:36', '2018-02-13 04:43:36'),
(465, 1, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:38', '2018-02-13 04:43:38'),
(466, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:41', '2018-02-13 04:43:41'),
(467, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:43:43', '2018-02-13 04:43:43'),
(468, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:44:01', '2018-02-13 04:44:01'),
(469, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:44:11', '2018-02-13 04:44:11'),
(470, 1, 'admin/auth/company/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-13 04:45:46', '2018-02-13 04:45:46'),
(471, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-13 04:47:25', '2018-02-13 04:47:25'),
(472, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-15 13:38:14', '2018-02-15 13:38:14'),
(473, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-15 13:38:34', '2018-02-15 13:38:34'),
(474, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-15 13:38:37', '2018-02-15 13:38:37'),
(475, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-15 13:39:49', '2018-02-15 13:39:49'),
(476, 1, 'admin/auth/products/1', 'PUT', '127.0.0.1', '{"name":"product_name","value":"Product B","pk":"1","_token":"AcC8wwcLHxkTWF6q0X6qiuvcfacUzsuCNZGFugaM","_editable":"1","_method":"PUT"}', '2018-02-15 13:39:59', '2018-02-15 13:39:59'),
(477, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-16 03:13:19', '2018-02-16 03:13:19'),
(478, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-16 03:19:20', '2018-02-16 03:19:20'),
(479, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-16 03:24:43', '2018-02-16 03:24:43'),
(480, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-16 03:32:41', '2018-02-16 03:32:41'),
(481, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:16:07', '2018-02-17 02:16:07'),
(482, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:17:43', '2018-02-17 02:17:43'),
(483, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:22:34', '2018-02-17 02:22:34'),
(484, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:25:58', '2018-02-17 02:25:58'),
(485, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:29:11', '2018-02-17 02:29:11'),
(486, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:29:45', '2018-02-17 02:29:45'),
(487, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:32:06', '2018-02-17 02:32:06'),
(488, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:32:33', '2018-02-17 02:32:33'),
(489, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:33:12', '2018-02-17 02:33:12'),
(490, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:34:02', '2018-02-17 02:34:02'),
(491, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:34:22', '2018-02-17 02:34:22'),
(492, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:44:21', '2018-02-17 02:44:21'),
(493, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:56:29', '2018-02-17 02:56:29'),
(494, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:56:52', '2018-02-17 02:56:52'),
(495, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:57:09', '2018-02-17 02:57:09'),
(496, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:57:15', '2018-02-17 02:57:15'),
(497, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 02:58:03', '2018-02-17 02:58:03'),
(498, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 03:00:26', '2018-02-17 03:00:26'),
(499, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 03:12:37', '2018-02-17 03:12:37'),
(500, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 03:12:56', '2018-02-17 03:12:56'),
(501, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 03:16:41', '2018-02-17 03:16:41'),
(502, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":null,"qty":null}],"_token":"Fqs3O8nYntyEhkfdGkfCt62QqUxhXjAYhEnwezvi"}', '2018-02-17 03:17:15', '2018-02-17 03:17:15'),
(503, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 14:45:56', '2018-02-17 14:45:56'),
(504, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 14:46:10', '2018-02-17 14:46:10'),
(505, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 14:46:43', '2018-02-17 14:46:43'),
(506, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 14:46:45', '2018-02-17 14:46:45'),
(507, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 14:46:51', '2018-02-17 14:46:51'),
(508, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-17 14:47:47', '2018-02-17 14:47:47'),
(509, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 14:48:49', '2018-02-17 14:48:49'),
(510, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 14:49:11', '2018-02-17 14:49:11'),
(511, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"},{"product_name":"1","qty":"1"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 15:39:07', '2018-02-17 15:39:07'),
(512, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-17 15:39:31', '2018-02-17 15:39:31'),
(513, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"},{"product_name":"1","qty":"1"}],"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 15:39:51', '2018-02-17 15:39:51'),
(514, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-17 15:40:24', '2018-02-17 15:40:24'),
(515, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":{"0":{"product_name":"1","qty":"12"},"2":{"product_name":"1","qty":"18"},"3":{"product_name":"1","qty":"2"}},"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 15:40:49', '2018-02-17 15:40:49'),
(516, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-17 15:41:36', '2018-02-17 15:41:36'),
(517, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":{"0":{"product_name":"1","qty":"12"},"2":{"product_name":"1","qty":"18"},"3":{"product_name":"1","qty":"2"}},"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 15:41:55', '2018-02-17 15:41:55'),
(518, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 15:42:27', '2018-02-17 15:42:27'),
(519, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-17 15:43:02', '2018-02-17 15:43:02'),
(520, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":{"0":{"product_name":"1","qty":"12"},"2":{"product_name":"1","qty":"18"},"3":{"product_name":"1","qty":"2"}},"_token":"b3JXOwdcvnw70oM01Zy0kUXf5gLuGujF4o6jjV2U"}', '2018-02-17 15:43:23', '2018-02-17 15:43:23'),
(521, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-17 15:44:25', '2018-02-17 15:44:25'),
(522, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 01:16:00', '2018-02-18 01:16:00'),
(523, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 01:16:28', '2018-02-18 01:16:28'),
(524, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-18 01:47:20', '2018-02-18 01:47:20'),
(525, 1, 'admin/auth/warehouse/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-18 01:47:26', '2018-02-18 01:47:26'),
(526, 1, 'admin/auth/warehouse/1', 'PUT', '127.0.0.1', '{"name":"Main Warehouse","address":"\\"Kuladeepa\\",Thalgamuwa, Devanagala","city":"City","post_code":"12000","phone_number":"0718068183_","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","status":"ACTIVE","_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/warehouse"}', '2018-02-18 01:47:36', '2018-02-18 01:47:36'),
(527, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-18 01:47:37', '2018-02-18 01:47:37'),
(528, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-18 03:31:36', '2018-02-18 03:31:36'),
(529, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:23:25', '2018-02-18 04:23:25'),
(530, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:23:57', '2018-02-18 04:23:57'),
(531, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-18 04:24:18', '2018-02-18 04:24:18'),
(532, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:25:51', '2018-02-18 04:25:51'),
(533, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:26:45', '2018-02-18 04:26:45'),
(534, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-18 04:26:51', '2018-02-18 04:26:51'),
(535, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:27:10', '2018-02-18 04:27:10'),
(536, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:27:17', '2018-02-18 04:27:17'),
(537, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:27:34', '2018-02-18 04:27:34'),
(538, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:34:14', '2018-02-18 04:34:14'),
(539, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:34:21', '2018-02-18 04:34:21'),
(540, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:35:03', '2018-02-18 04:35:03'),
(541, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-18 04:35:06', '2018-02-18 04:35:06'),
(542, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:35:19', '2018-02-18 04:35:19'),
(543, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:35:25', '2018-02-18 04:35:25'),
(544, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:36:04', '2018-02-18 04:36:04'),
(545, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:39:15', '2018-02-18 04:39:15'),
(546, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"12"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:39:21', '2018-02-18 04:39:21'),
(547, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-18 04:39:35', '2018-02-18 04:39:35'),
(548, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:45:08', '2018-02-18 04:45:08'),
(549, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"1000"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:45:14', '2018-02-18 04:45:14'),
(550, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-18 04:45:26', '2018-02-18 04:45:26'),
(551, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-18 04:49:31', '2018-02-18 04:49:31'),
(552, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"35"}],"_token":"PE76QkXchXK8K7NSe2hSyN2VR4RxOGehJsOD8YKs"}', '2018-02-18 04:49:40', '2018-02-18 04:49:40'),
(553, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-18 04:49:51', '2018-02-18 04:49:51'),
(554, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 03:53:09', '2018-02-19 03:53:09'),
(555, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:14:20', '2018-02-19 04:14:20'),
(556, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"150"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:14:36', '2018-02-19 04:14:36'),
(557, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-19 04:14:38', '2018-02-19 04:14:38'),
(558, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:14:50', '2018-02-19 04:14:50'),
(559, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":null,"qty":"100"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:15:00', '2018-02-19 04:15:00'),
(560, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:15:48', '2018-02-19 04:15:48'),
(561, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:16:04', '2018-02-19 04:16:04'),
(562, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"11"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:16:09', '2018-02-19 04:16:09'),
(563, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-19 04:20:10', '2018-02-19 04:20:10'),
(564, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:20:24', '2018-02-19 04:20:24'),
(565, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:20:27', '2018-02-19 04:20:27'),
(566, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":"Product A","sku":"123","description":null,"product_type_id":null,"supplier_id":null,"brand_id":null,"cost":null,"buying_price":null,"wholesale_price":null,"retail_price":null,"manage_stock_level":"YES","re_order_level":null,"category_id":"0","status":"ACTIVE","_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/products"}', '2018-02-19 04:20:36', '2018-02-19 04:20:36'),
(567, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '[]', '2018-02-19 04:20:37', '2018-02-19 04:20:37'),
(568, 1, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":"Product A","sku":"S","description":null,"product_type_id":"1","supplier_id":null,"brand_id":null,"cost":null,"buying_price":null,"wholesale_price":null,"retail_price":"110","manage_stock_level":"YES","re_order_level":"10","category_id":"2","status":"ACTIVE","_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:21:04', '2018-02-19 04:21:04'),
(569, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-19 04:21:04', '2018-02-19 04:21:04'),
(570, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:21:11', '2018-02-19 04:21:11'),
(571, 1, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:21:11', '2018-02-19 04:21:11'),
(572, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:21:12', '2018-02-19 04:21:12'),
(573, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 04:21:14', '2018-02-19 04:21:14'),
(574, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:22:17', '2018-02-19 04:22:17'),
(575, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"100"},{"product_name":"2","qty":"11"},{"product_name":null,"qty":null}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:22:41', '2018-02-19 04:22:41'),
(576, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:23:46', '2018-02-19 04:23:46'),
(577, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:25:39', '2018-02-19 04:25:39'),
(578, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"5"},{"product_name":"2","qty":"100"},{"product_name":null,"qty":null}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:27:13', '2018-02-19 04:27:13'),
(579, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-19 04:29:52', '2018-02-19 04:29:52'),
(580, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:36:26', '2018-02-19 04:36:26'),
(581, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"fields":[{"product_name":"1","qty":"1"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:36:35', '2018-02-19 04:36:35'),
(582, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:41:18', '2018-02-19 04:41:18'),
(583, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:43:15', '2018-02-19 04:43:15'),
(584, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:43:26', '2018-02-19 04:43:26'),
(585, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:44:05', '2018-02-19 04:44:05'),
(586, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:44:33', '2018-02-19 04:44:33'),
(587, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:45:14', '2018-02-19 04:45:14'),
(588, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:49:01', '2018-02-19 04:49:01'),
(589, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:51:08', '2018-02-19 04:51:08'),
(590, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:52:12', '2018-02-19 04:52:12'),
(591, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"fields":[{"product_name":null,"qty":null}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 04:52:14', '2018-02-19 04:52:14'),
(592, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 04:53:00', '2018-02-19 04:53:00'),
(593, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:08:47', '2018-02-19 05:08:47'),
(594, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"Description","fields":[{"product_name":"1","qty":"12"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:09:08', '2018-02-19 05:09:08'),
(595, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:09:35', '2018-02-19 05:09:35'),
(596, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:11:09', '2018-02-19 05:11:09'),
(597, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"Description","fields":[{"product_name":"1","qty":"12"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:11:28', '2018-02-19 05:11:28'),
(598, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:12:11', '2018-02-19 05:12:11'),
(599, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:13:12', '2018-02-19 05:13:12'),
(600, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:13:50', '2018-02-19 05:13:50'),
(601, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-1","description":"Desc","fields":[{"product_name":"1","qty":"12"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:14:11', '2018-02-19 05:14:11'),
(602, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:14:43', '2018-02-19 05:14:43'),
(603, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:15:51', '2018-02-19 05:15:51'),
(604, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-1","description":"Desc","fields":[{"product_name":"1","qty":"12"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:16:07', '2018-02-19 05:16:07'),
(605, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:16:55', '2018-02-19 05:16:55'),
(606, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:18:33', '2018-02-19 05:18:33'),
(607, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-1","description":"Desc","fields":[{"product_name":"1","qty":"10"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:18:45', '2018-02-19 05:18:45'),
(608, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:42:59', '2018-02-19 05:42:59'),
(609, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:43:23', '2018-02-19 05:43:23'),
(610, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:43:54', '2018-02-19 05:43:54'),
(611, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:45:06', '2018-02-19 05:45:06'),
(612, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:45:33', '2018-02-19 05:45:33'),
(613, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:45:39', '2018-02-19 05:45:39'),
(614, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:45:40', '2018-02-19 05:45:40'),
(615, 1, 'admin/auth/currency', 'GET', '127.0.0.1', '[]', '2018-02-19 05:45:54', '2018-02-19 05:45:54'),
(616, 1, 'admin/auth/currency/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:45:57', '2018-02-19 05:45:57'),
(617, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:46:31', '2018-02-19 05:46:31'),
(618, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:46:34', '2018-02-19 05:46:34'),
(619, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-2","description":"Desc","fields":[{"product_name":"1","qty":"10"},{"product_name":"2","qty":"100"}],"_token":"Oi4Meapy98xypEKAE6D5QC07E4yoWRKRCP56kLnM"}', '2018-02-19 05:47:07', '2018-02-19 05:47:07'),
(620, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-19 05:47:15', '2018-02-19 05:47:15'),
(621, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:47:31', '2018-02-19 05:47:31'),
(622, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:49:01', '2018-02-19 05:49:01'),
(623, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:49:09', '2018-02-19 05:49:09'),
(624, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:49:44', '2018-02-19 05:49:44'),
(625, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:52:46', '2018-02-19 05:52:46'),
(626, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:52:49', '2018-02-19 05:52:49'),
(627, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:52:50', '2018-02-19 05:52:50'),
(628, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:53:22', '2018-02-19 05:53:22'),
(629, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:53:28', '2018-02-19 05:53:28'),
(630, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:53:29', '2018-02-19 05:53:29'),
(631, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:53:59', '2018-02-19 05:53:59'),
(632, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:54:01', '2018-02-19 05:54:01'),
(633, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:54:02', '2018-02-19 05:54:02'),
(634, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:56:01', '2018-02-19 05:56:01'),
(635, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:56:05', '2018-02-19 05:56:05'),
(636, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:56:07', '2018-02-19 05:56:07'),
(637, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 05:57:03', '2018-02-19 05:57:03'),
(638, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:57:03', '2018-02-19 05:57:03'),
(639, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 05:57:45', '2018-02-19 05:57:45'),
(640, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:00:11', '2018-02-19 06:00:11'),
(641, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:01:26', '2018-02-19 06:01:26'),
(642, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:01:59', '2018-02-19 06:01:59'),
(643, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:02:17', '2018-02-19 06:02:17'),
(644, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:02:52', '2018-02-19 06:02:52'),
(645, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 06:03:00', '2018-02-19 06:03:00');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(646, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:03:01', '2018-02-19 06:03:01'),
(647, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:08:15', '2018-02-19 06:08:15'),
(648, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:10:39', '2018-02-19 06:10:39'),
(649, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:13:38', '2018-02-19 06:13:38'),
(650, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:14:06', '2018-02-19 06:14:06'),
(651, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:14:38', '2018-02-19 06:14:38'),
(652, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:15:48', '2018-02-19 06:15:48'),
(653, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:23:08', '2018-02-19 06:23:08'),
(654, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:39:27', '2018-02-19 06:39:27'),
(655, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:42:15', '2018-02-19 06:42:15'),
(656, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"id":"1","_pjax":"#pjax-container"}', '2018-02-19 06:42:18', '2018-02-19 06:42:18'),
(657, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:42:19', '2018-02-19 06:42:19'),
(658, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:42:42', '2018-02-19 06:42:42'),
(659, 1, 'admin/auth/grn/items', 'GET', '127.0.0.1', '{"id":"2","_pjax":"#pjax-container"}', '2018-02-19 06:42:47', '2018-02-19 06:42:47'),
(660, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:42:47', '2018-02-19 06:42:47'),
(661, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 06:42:59', '2018-02-19 06:42:59'),
(662, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-19 06:51:16', '2018-02-19 06:51:16'),
(663, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-19 06:51:56', '2018-02-19 06:51:56'),
(664, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-19 06:52:20', '2018-02-19 06:52:20'),
(665, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-19 06:52:51', '2018-02-19 06:52:51'),
(666, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-19 06:58:38', '2018-02-19 06:58:38'),
(667, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 23:08:13', '2018-02-19 23:08:13'),
(668, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 23:15:21', '2018-02-19 23:15:21'),
(669, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-19 23:16:36', '2018-02-19 23:16:36'),
(670, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-19 23:17:09', '2018-02-19 23:17:09'),
(671, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-19 23:17:43', '2018-02-19 23:17:43'),
(672, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-19 23:18:57', '2018-02-19 23:18:57'),
(673, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-19 23:34:46', '2018-02-19 23:34:46'),
(674, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-19 23:35:42', '2018-02-19 23:35:42'),
(675, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 23:35:58', '2018-02-19 23:35:58'),
(676, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:36:27', '2018-02-19 23:36:27'),
(677, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:38:40', '2018-02-19 23:38:40'),
(678, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:39:01', '2018-02-19 23:39:01'),
(679, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:39:18', '2018-02-19 23:39:18'),
(680, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:46:21', '2018-02-19 23:46:21'),
(681, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:46:36', '2018-02-19 23:46:36'),
(682, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:48:51', '2018-02-19 23:48:51'),
(683, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:49:02', '2018-02-19 23:49:02'),
(684, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:49:30', '2018-02-19 23:49:30'),
(685, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:53:07', '2018-02-19 23:53:07'),
(686, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:53:21', '2018-02-19 23:53:21'),
(687, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:54:29', '2018-02-19 23:54:29'),
(688, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:54:36', '2018-02-19 23:54:36'),
(689, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-19 23:54:55', '2018-02-19 23:54:55'),
(690, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 23:55:07', '2018-02-19 23:55:07'),
(691, 1, 'admin/auth/pricelists/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 23:55:16', '2018-02-19 23:55:16'),
(692, 1, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 23:59:33', '2018-02-19 23:59:33'),
(693, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-19 23:59:34', '2018-02-19 23:59:34'),
(694, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-20 00:00:06', '2018-02-20 00:00:06'),
(695, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-20 00:00:47', '2018-02-20 00:00:47'),
(696, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '[]', '2018-02-20 00:01:18', '2018-02-20 00:01:18'),
(697, 1, 'admin/auth/grn/items/2', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 01:58:39', '2018-02-20 01:58:39'),
(698, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-20 01:59:05', '2018-02-20 01:59:05'),
(699, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 01:59:11', '2018-02-20 01:59:11'),
(700, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"0","title":"Inventory","icon":"fa-cubes","uri":null,"roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:00:34', '2018-02-20 02:00:34'),
(701, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 02:00:34', '2018-02-20 02:00:34'),
(702, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 02:00:41', '2018-02-20 02:00:41'),
(703, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-20 02:01:23', '2018-02-20 02:01:23'),
(704, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"24","title":"Manage GRN","icon":"fa-bars","uri":"auth\\/grn","roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:01:47', '2018-02-20 02:01:47'),
(705, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 02:01:48', '2018-02-20 02:01:48'),
(706, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 02:02:18', '2018-02-20 02:02:18'),
(707, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 02:02:24', '2018-02-20 02:02:24'),
(708, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-20 02:02:34', '2018-02-20 02:02:34'),
(709, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-20 02:04:19', '2018-02-20 02:04:19'),
(710, 1, 'admin/auth/grn/items/1', 'GET', '127.0.0.1', '[]', '2018-02-20 02:04:42', '2018-02-20 02:04:42'),
(711, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 02:04:51', '2018-02-20 02:04:51'),
(712, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '[]', '2018-02-20 02:14:25', '2018-02-20 02:14:25'),
(713, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 02:14:28', '2018-02-20 02:14:28'),
(714, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:14:54', '2018-02-20 02:14:54'),
(715, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:14:54', '2018-02-20 02:14:54'),
(716, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:18:50', '2018-02-20 02:18:50'),
(717, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:18:53', '2018-02-20 02:18:53'),
(718, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:18:58', '2018-02-20 02:18:58'),
(719, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:19:00', '2018-02-20 02:19:00'),
(720, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:19:21', '2018-02-20 02:19:21'),
(721, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:19:24', '2018-02-20 02:19:24'),
(722, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:19:25', '2018-02-20 02:19:25'),
(723, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:19:26', '2018-02-20 02:19:26'),
(724, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:19:43', '2018-02-20 02:19:43'),
(725, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-3","description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:20:31', '2018-02-20 02:20:31'),
(726, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:20:40', '2018-02-20 02:20:40'),
(727, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:25:36', '2018-02-20 02:25:36'),
(728, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:26:06', '2018-02-20 02:26:06'),
(729, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:26:15', '2018-02-20 02:26:15'),
(730, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"asdasdasdas","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:26:29', '2018-02-20 02:26:29'),
(731, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"asdasdasdas","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:26:32', '2018-02-20 02:26:32'),
(732, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:26:37', '2018-02-20 02:26:37'),
(733, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:26:48', '2018-02-20 02:26:48'),
(734, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":null,"fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:26:55', '2018-02-20 02:26:55'),
(735, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:27:16', '2018-02-20 02:27:16'),
(736, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"Desc","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:27:29', '2018-02-20 02:27:29'),
(737, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:27:57', '2018-02-20 02:27:57'),
(738, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 02:28:01', '2018-02-20 02:28:01'),
(739, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"Desc","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:28:06', '2018-02-20 02:28:06'),
(740, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"Desc","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:33:45', '2018-02-20 02:33:45'),
(741, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":null,"description":"Desc","fields":[{"product_name":null,"qty":null}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 02:39:44', '2018-02-20 02:39:44'),
(742, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:02:49', '2018-02-20 03:02:49'),
(743, 1, 'admin/auth/grn/create', 'POST', '127.0.0.1', '{"grn_number":"GR-3","description":"Desc","fields":[{"product_name":"1","qty":"100"},{"product_name":"2","qty":"50"}],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 03:03:43', '2018-02-20 03:03:43'),
(744, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:03:52', '2018-02-20 03:03:52'),
(745, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:04:00', '2018-02-20 03:04:00'),
(746, 1, 'admin/auth/grn/items/3', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:04:03', '2018-02-20 03:04:03'),
(747, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:04:50', '2018-02-20 03:04:50'),
(748, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 03:22:10', '2018-02-20 03:22:10'),
(749, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 03:23:23', '2018-02-20 03:23:23'),
(750, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:23:30', '2018-02-20 03:23:30'),
(751, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:23:44', '2018-02-20 03:23:44'),
(752, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:23:48', '2018-02-20 03:23:48'),
(753, 1, 'admin/auth/warehouse', 'POST', '127.0.0.1', '{"name":"Warehouse 1","address":"Address 1, Address 2","city":"Colombo","post_code":"12000","phone_number":"718068183__","email_address":"arangaw@gmail.com","contact_person":"Aranga Wijesooriya","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/warehouse"}', '2018-02-20 03:24:20', '2018-02-20 03:24:20'),
(754, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '[]', '2018-02-20 03:24:21', '2018-02-20 03:24:21'),
(755, 1, 'admin/auth/warehouse/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:24:25', '2018-02-20 03:24:25'),
(756, 1, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:24:28', '2018-02-20 03:24:28'),
(757, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 03:24:29', '2018-02-20 03:24:29'),
(758, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:25:15', '2018-02-20 03:25:15'),
(759, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:25:56', '2018-02-20 03:25:56'),
(760, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":null,"to_warehouse_id":null,"product_id":null,"qty":null,"description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 03:26:35', '2018-02-20 03:26:35'),
(761, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:26:35', '2018-02-20 03:26:35'),
(762, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:26:58', '2018-02-20 03:26:58'),
(763, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 03:41:03', '2018-02-20 03:41:03'),
(764, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"30","description":"Desc","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 03:41:58', '2018-02-20 03:41:58'),
(765, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 03:42:45', '2018-02-20 03:42:45'),
(766, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 04:13:18', '2018-02-20 04:13:18'),
(767, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 04:14:28', '2018-02-20 04:14:28'),
(768, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_adjustment"}', '2018-02-20 04:14:51', '2018-02-20 04:14:51'),
(769, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 04:15:33', '2018-02-20 04:15:33'),
(770, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 04:16:00', '2018-02-20 04:16:00'),
(771, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 04:16:18', '2018-02-20 04:16:18'),
(772, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 04:17:54', '2018-02-20 04:17:54'),
(773, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 04:18:59', '2018-02-20 04:18:59'),
(774, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_adjustment"}', '2018-02-20 04:19:18', '2018-02-20 04:19:18'),
(775, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 04:19:41', '2018-02-20 04:19:41'),
(776, 1, 'admin/auth/stock_adjustment/create', 'GET', '127.0.0.1', '[]', '2018-02-20 04:19:55', '2018-02-20 04:19:55'),
(777, 1, 'admin/auth/stock_adjustment', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_adjustment"}', '2018-02-20 04:20:10', '2018-02-20 04:20:10'),
(778, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '[]', '2018-02-20 04:20:34', '2018-02-20 04:20:34'),
(779, 1, 'admin/auth/stock_adjustment/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 04:20:53', '2018-02-20 04:20:53'),
(780, 1, 'admin/auth/stock_adjustment', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 04:21:08', '2018-02-20 04:21:08'),
(781, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:31:38', '2018-02-20 04:31:38'),
(782, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:33:01', '2018-02-20 04:33:01'),
(783, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 04:33:05', '2018-02-20 04:33:05'),
(784, 1, 'admin/auth/stock_transfer', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":"Desc","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 04:33:39', '2018-02-20 04:33:39'),
(785, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:33:43', '2018-02-20 04:33:43'),
(786, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:34:30', '2018-02-20 04:34:30'),
(787, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:35:52', '2018-02-20 04:35:52'),
(788, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 04:36:23', '2018-02-20 04:36:23'),
(789, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:40:48', '2018-02-20 04:40:48'),
(790, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:41:24', '2018-02-20 04:41:24'),
(791, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:41:48', '2018-02-20 04:41:48'),
(792, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:45:54', '2018-02-20 04:45:54'),
(793, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:46:18', '2018-02-20 04:46:18'),
(794, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":null,"warehouse":null,"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 04:46:21', '2018-02-20 04:46:21'),
(795, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:46:22', '2018-02-20 04:46:22'),
(796, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:58:55', '2018-02-20 04:58:55'),
(797, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:59:07', '2018-02-20 04:59:07'),
(798, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:59:38', '2018-02-20 04:59:38'),
(799, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 04:59:54', '2018-02-20 04:59:54'),
(800, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:00:16', '2018-02-20 05:00:16'),
(801, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:00:36', '2018-02-20 05:00:36'),
(802, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:01:23', '2018-02-20 05:01:23'),
(803, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:01:34', '2018-02-20 05:01:34'),
(804, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:01:41', '2018-02-20 05:01:41'),
(805, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:01:55', '2018-02-20 05:01:55'),
(806, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:02:02', '2018-02-20 05:02:02'),
(807, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:02:42', '2018-02-20 05:02:42'),
(808, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:02:48', '2018-02-20 05:02:48'),
(809, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:05:29', '2018-02-20 05:05:29'),
(810, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:05:37', '2018-02-20 05:05:37'),
(811, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:05:44', '2018-02-20 05:05:44'),
(812, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:07:37', '2018-02-20 05:07:37'),
(813, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:07:45', '2018-02-20 05:07:45'),
(814, 1, 'admin/auth/stock_transfer', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"10","description":"Desc","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 05:08:13', '2018-02-20 05:08:13'),
(815, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:10:17', '2018-02-20 05:10:17'),
(816, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:12:00', '2018-02-20 05:12:00'),
(817, 1, 'admin/auth/stock_transfer', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"2","qty":"2","description":"Desc 12","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 05:12:14', '2018-02-20 05:12:14'),
(818, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:12:34', '2018-02-20 05:12:34'),
(819, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:12:55', '2018-02-20 05:12:55'),
(820, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:13:05', '2018-02-20 05:13:05'),
(821, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:13:15', '2018-02-20 05:13:15'),
(822, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"2","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:13:22', '2018-02-20 05:13:22'),
(823, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:14:08', '2018-02-20 05:14:08'),
(824, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:14:24', '2018-02-20 05:14:24'),
(825, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:15:36', '2018-02-20 05:15:36'),
(826, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:15:44', '2018-02-20 05:15:44'),
(827, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:18:20', '2018-02-20 05:18:20'),
(828, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:18:26', '2018-02-20 05:18:26'),
(829, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:18:31', '2018-02-20 05:18:31'),
(830, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:18:32', '2018-02-20 05:18:32'),
(831, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:21:19', '2018-02-20 05:21:19'),
(832, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:21:27', '2018-02-20 05:21:27'),
(833, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:22:06', '2018-02-20 05:22:06'),
(834, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"2","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:22:12', '2018-02-20 05:22:12'),
(835, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:24:08', '2018-02-20 05:24:08'),
(836, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"2","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:24:14', '2018-02-20 05:24:14'),
(837, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '[]', '2018-02-20 05:28:25', '2018-02-20 05:28:25'),
(838, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '[]', '2018-02-20 05:28:57', '2018-02-20 05:28:57'),
(839, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '[]', '2018-02-20 05:29:22', '2018-02-20 05:29:22'),
(840, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '[]', '2018-02-20 05:29:34', '2018-02-20 05:29:34'),
(841, 1, 'admin/auth/create', 'POST', '127.0.0.1', '{"product_name":"2","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:29:41', '2018-02-20 05:29:41'),
(842, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '[]', '2018-02-20 05:33:37', '2018-02-20 05:33:37'),
(843, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '[]', '2018-02-20 05:33:42', '2018-02-20 05:33:42'),
(844, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":null,"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:33:50', '2018-02-20 05:33:50'),
(845, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":null,"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:35:32', '2018-02-20 05:35:32'),
(846, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:35:42', '2018-02-20 05:35:42'),
(847, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":null,"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:35:46', '2018-02-20 05:35:46'),
(848, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:37:34', '2018-02-20 05:37:34'),
(849, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:37:47', '2018-02-20 05:37:47'),
(850, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:38:08', '2018-02-20 05:38:08'),
(851, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:38:14', '2018-02-20 05:38:14'),
(852, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:42:10', '2018-02-20 05:42:10'),
(853, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:42:16', '2018-02-20 05:42:16'),
(854, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:48:16', '2018-02-20 05:48:16'),
(855, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"24","title":"Stock Transfer","icon":"fa-bars","uri":"auth\\/stock_transfer","roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:49:20', '2018-02-20 05:49:20'),
(856, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:49:21', '2018-02-20 05:49:21'),
(857, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:49:25', '2018-02-20 05:49:25'),
(858, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:49:28', '2018-02-20 05:49:28'),
(859, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:49:32', '2018-02-20 05:49:32'),
(860, 1, 'admin/auth/stock_transfer', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"20","description":"Desc12","status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 05:49:53', '2018-02-20 05:49:53'),
(861, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:49:59', '2018-02-20 05:49:59'),
(862, 1, 'admin/auth/stock_transfer/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:50:19', '2018-02-20 05:50:19'),
(863, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:50:25', '2018-02-20 05:50:25'),
(864, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:50:27', '2018-02-20 05:50:27'),
(865, 1, 'admin/auth/stock_transfer', 'POST', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"2","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 05:50:39', '2018-02-20 05:50:39'),
(866, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:51:46', '2018-02-20 05:51:46'),
(867, 1, 'admin/auth/stock_transfer/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:51:52', '2018-02-20 05:51:52'),
(868, 1, 'admin/auth/stock_transfer/5', 'PUT', '127.0.0.1', '{"from_warehouse_id":"1","to_warehouse_id":"2","product_id":"1","qty":"2.00","description":null,"status":"ACTIVE","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/stock_transfer"}', '2018-02-20 05:51:54', '2018-02-20 05:51:54'),
(869, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-20 05:52:10', '2018-02-20 05:52:10'),
(870, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:53:16', '2018-02-20 05:53:16'),
(871, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"0","title":"Reports","icon":"fa-bars","uri":null,"roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:53:28', '2018-02-20 05:53:28'),
(872, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:53:29', '2018-02-20 05:53:29'),
(873, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"27","title":"Bin Card","icon":"fa-bars","uri":"auth\\/bincard","roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:54:35', '2018-02-20 05:54:35'),
(874, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:54:36', '2018-02-20 05:54:36'),
(875, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"27","title":"Stock","icon":"fa-bars","uri":"auth\\/stock","roles":["1",null],"_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:55:21', '2018-02-20 05:55:21'),
(876, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:55:21', '2018-02-20 05:55:21'),
(877, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:55:23', '2018-02-20 05:55:23'),
(878, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-20 05:55:25', '2018-02-20 05:55:25'),
(879, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-20 05:55:30', '2018-02-20 05:55:30'),
(880, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"product_name":"1","warehouse":"1","_token":"jf395C0iHqZTnvo1e538FiPhFBxdXXhEhzlAEjWk"}', '2018-02-20 05:55:36', '2018-02-20 05:55:36'),
(881, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:05:11', '2018-02-21 01:05:11'),
(882, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:05:41', '2018-02-21 01:05:41'),
(883, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:06:10', '2018-02-21 01:06:10'),
(884, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:06:19', '2018-02-21 01:06:19'),
(885, 1, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:06:27', '2018-02-21 01:06:27'),
(886, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:06:53', '2018-02-21 01:06:53'),
(887, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:07:00', '2018-02-21 01:07:00'),
(888, 1, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:07:05', '2018-02-21 01:07:05'),
(889, 1, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:07:07', '2018-02-21 01:07:07'),
(890, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:07:31', '2018-02-21 01:07:31'),
(891, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-02-21 01:08:51', '2018-02-21 01:08:51'),
(892, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:08:54', '2018-02-21 01:08:54'),
(893, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:11:45', '2018-02-21 01:11:45'),
(894, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{"username":"reviva","first_name":"Reviva","last_name":"Administrator","email":"admin@revivatea.com","telephone":"0718068183","password":"admin","password_confirmation":"admin","roles":["1",null],"permissions":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ"}', '2018-02-21 01:12:54', '2018-02-21 01:12:54'),
(895, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:12:57', '2018-02-21 01:12:57'),
(896, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{"username":"reviva","first_name":"Reviva","last_name":"Administrator","email":"admin@revivatea.com","telephone":"0718068183","password":"admin","password_confirmation":"admin","roles":["1",null],"permissions":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ"}', '2018-02-21 01:13:29', '2018-02-21 01:13:29'),
(897, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:15:00', '2018-02-21 01:15:00'),
(898, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:15:12', '2018-02-21 01:15:12'),
(899, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{"username":"revivatea","first_name":"Reviva","last_name":"Administrator","email":"admin@admin.com","telephone":"0718068183","password":"admin","password_confirmation":"admin","roles":["1",null],"permissions":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ"}', '2018-02-21 01:15:56', '2018-02-21 01:15:56'),
(900, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:16:04', '2018-02-21 01:16:04'),
(901, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:21:02', '2018-02-21 01:21:02'),
(902, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:21:08', '2018-02-21 01:21:08'),
(903, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:22:18', '2018-02-21 01:22:18'),
(904, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:22:59', '2018-02-21 01:22:59'),
(905, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:23:17', '2018-02-21 01:23:17'),
(906, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:23:40', '2018-02-21 01:23:40'),
(907, 1, 'admin/auth/permissions/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:23:44', '2018-02-21 01:23:44'),
(908, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{"slug":"reviva.management","name":"Reviva Management","http_method":[null],"http_path":"\\/auth\\/roles\\r\\n\\/auth\\/permissions","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 01:24:11', '2018-02-21 01:24:11'),
(909, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:24:11', '2018-02-21 01:24:11'),
(910, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:24:39', '2018-02-21 01:24:39'),
(911, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{"slug":"auth.configuration","name":"Auth Configuration","http_method":[null],"http_path":"\\/auth\\/currency\\r\\n\\/auth\\/pricelists\\r\\n\\/auth\\/tax\\r\\n\\/auth\\/paymentterms\\r\\n\\/auth\\/paymentmethods\\r\\n\\/auth\\/stockadjustmentreasons\\r\\n\\/auth\\/suppliers\\r\\n\\/auth\\/company","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 01:26:49', '2018-02-21 01:26:49'),
(912, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:26:50', '2018-02-21 01:26:50'),
(913, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:27:00', '2018-02-21 01:27:00'),
(914, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{"slug":"auth.reports","name":"Auth Reports","http_method":[null],"http_path":"\\/auth\\/stock\\r\\n\\/auth\\/bincard","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 01:27:27', '2018-02-21 01:27:27'),
(915, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:27:27', '2018-02-21 01:27:27'),
(916, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:27:31', '2018-02-21 01:27:31'),
(917, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{"slug":"auth.inventory","name":"Auth Inventory","http_method":[null],"http_path":"\\/auth\\/stock_transfer\\r\\n\\/auth\\/grn","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 01:28:03', '2018-02-21 01:28:03'),
(918, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:28:04', '2018-02-21 01:28:04'),
(919, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '[]', '2018-02-21 01:28:12', '2018-02-21 01:28:12'),
(920, 1, 'admin/auth/roles', 'POST', '127.0.0.1', '{"slug":"reviva.admin","name":"Reviva Administrator","permissions":["2","3","4","6","7","8","9",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ"}', '2018-02-21 01:28:49', '2018-02-21 01:28:49'),
(921, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2018-02-21 01:28:50', '2018-02-21 01:28:50'),
(922, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:29:12', '2018-02-21 01:29:12'),
(923, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:29:14', '2018-02-21 01:29:14'),
(924, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:11', '2018-02-21 01:32:11'),
(925, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:21', '2018-02-21 01:32:21'),
(926, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:44', '2018-02-21 01:32:44'),
(927, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:32:52', '2018-02-21 01:32:52'),
(928, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:55', '2018-02-21 01:32:55'),
(929, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:56', '2018-02-21 01:32:56'),
(930, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:32:58', '2018-02-21 01:32:58'),
(931, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:01', '2018-02-21 01:33:01'),
(932, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:02', '2018-02-21 01:33:02'),
(933, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:07', '2018-02-21 01:33:07'),
(934, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:33:12', '2018-02-21 01:33:12'),
(935, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:23', '2018-02-21 01:33:23'),
(936, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:27', '2018-02-21 01:33:27'),
(937, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:33:29', '2018-02-21 01:33:29'),
(938, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:10', '2018-02-21 01:34:10'),
(939, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:19', '2018-02-21 01:34:19'),
(940, 1, 'admin/auth/roles/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:22', '2018-02-21 01:34:22'),
(941, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:27', '2018-02-21 01:34:27'),
(942, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:29', '2018-02-21 01:34:29'),
(943, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:31', '2018-02-21 01:34:31'),
(944, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:44', '2018-02-21 01:34:44'),
(945, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:34:58', '2018-02-21 01:34:58'),
(946, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:35:00', '2018-02-21 01:35:00'),
(947, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{"username":"radmin","first_name":"Reviva","last_name":"Administrator","email":"admin@reviva.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["2",null],"permissions":[null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-02-21 01:35:22', '2018-02-21 01:35:22'),
(948, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-02-21 01:35:28', '2018-02-21 01:35:28'),
(949, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:35:50', '2018-02-21 01:35:50'),
(950, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:35:56', '2018-02-21 01:35:56'),
(951, 2, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:36:00', '2018-02-21 01:36:00'),
(952, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:36:09', '2018-02-21 01:36:09'),
(953, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:36:15', '2018-02-21 01:36:15'),
(954, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:36:32', '2018-02-21 01:36:32'),
(955, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:37:15', '2018-02-21 01:37:15'),
(956, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:37:20', '2018-02-21 01:37:20'),
(957, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:39:06', '2018-02-21 01:39:06'),
(958, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:39:08', '2018-02-21 01:39:08'),
(959, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:39:17', '2018-02-21 01:39:17'),
(960, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:39:20', '2018-02-21 01:39:20'),
(961, 1, 'admin/auth/roles/2', 'PUT', '127.0.0.1', '{"slug":"reviva.admin","name":"Reviva Administrator","permissions":["1","2","3","4","6","7","8","9",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/roles"}', '2018-02-21 01:39:29', '2018-02-21 01:39:29'),
(962, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2018-02-21 01:39:30', '2018-02-21 01:39:30'),
(963, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:39:35', '2018-02-21 01:39:35'),
(964, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:39:41', '2018-02-21 01:39:41'),
(965, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:40:04', '2018-02-21 01:40:04'),
(966, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:40:12', '2018-02-21 01:40:12'),
(967, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:40:18', '2018-02-21 01:40:18'),
(968, 1, 'admin/auth/roles/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:40:33', '2018-02-21 01:40:33'),
(969, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:40:45', '2018-02-21 01:40:45'),
(970, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:40:48', '2018-02-21 01:40:48'),
(971, 1, 'admin/auth/roles/2', 'PUT', '127.0.0.1', '{"slug":"reviva.admin","name":"Reviva Administrator","permissions":["2","3","4","6","7","8","9",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/roles"}', '2018-02-21 01:42:20', '2018-02-21 01:42:20'),
(972, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2018-02-21 01:42:21', '2018-02-21 01:42:21'),
(973, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:44:10', '2018-02-21 01:44:10'),
(974, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:44:20', '2018-02-21 01:44:20'),
(975, 1, 'admin/auth/menu/2', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"Admin","icon":"fa-tasks","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:44:28', '2018-02-21 01:44:28'),
(976, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:44:28', '2018-02-21 01:44:28'),
(977, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:44:33', '2018-02-21 01:44:33'),
(978, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:44:42', '2018-02-21 01:44:42'),
(979, 1, 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:44:57', '2018-02-21 01:44:57'),
(980, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:00', '2018-02-21 01:45:00'),
(981, 1, 'admin/auth/menu/7/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:04', '2018-02-21 01:45:04'),
(982, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:07', '2018-02-21 01:45:07'),
(983, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:21', '2018-02-21 01:45:21'),
(984, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"System","icon":"fa-toggle-on","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:45:26', '2018-02-21 01:45:26'),
(985, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:45:26', '2018-02-21 01:45:26'),
(986, 1, 'admin/auth/menu/15/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:33', '2018-02-21 01:45:33'),
(987, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:36', '2018-02-21 01:45:36'),
(988, 1, 'admin/auth/menu/27/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:45:53', '2018-02-21 01:45:53');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(989, 1, 'admin/auth/menu/27', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"Reports","icon":"fa-bars","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:45:57', '2018-02-21 01:45:57'),
(990, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:45:58', '2018-02-21 01:45:58'),
(991, 1, 'admin/auth/menu/24/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:46:02', '2018-02-21 01:46:02'),
(992, 1, 'admin/auth/menu/24', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"Inventory","icon":"fa-cubes","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:46:06', '2018-02-21 01:46:06'),
(993, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:46:06', '2018-02-21 01:46:06'),
(994, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:46:13', '2018-02-21 01:46:13'),
(995, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:46:21', '2018-02-21 01:46:21'),
(996, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:46:39', '2018-02-21 01:46:39'),
(997, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:46:46', '2018-02-21 01:46:46'),
(998, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:46:55', '2018-02-21 01:46:55'),
(999, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:46:59', '2018-02-21 01:46:59'),
(1000, 1, 'admin/auth/menu/2', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"Admin","icon":"fa-tasks","uri":null,"roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:47:07', '2018-02-21 01:47:07'),
(1001, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:47:07', '2018-02-21 01:47:07'),
(1002, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:47:15', '2018-02-21 01:47:15'),
(1003, 1, 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Users","icon":"fa-users","uri":"auth\\/users","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:47:21', '2018-02-21 01:47:21'),
(1004, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:47:21', '2018-02-21 01:47:21'),
(1005, 1, 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:47:27', '2018-02-21 01:47:27'),
(1006, 1, 'admin/auth/menu/4', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Roles","icon":"fa-user","uri":"auth\\/roles","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:47:36', '2018-02-21 01:47:36'),
(1007, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:47:36', '2018-02-21 01:47:36'),
(1008, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:47:57', '2018-02-21 01:47:57'),
(1009, 1, 'admin/auth/menu/5', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Permission","icon":"fa-ban","uri":"auth\\/permissions","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:48:02', '2018-02-21 01:48:02'),
(1010, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:48:03', '2018-02-21 01:48:03'),
(1011, 1, 'admin/auth/menu/15/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:48:17', '2018-02-21 01:48:17'),
(1012, 1, 'admin/auth/menu/15', 'PUT', '127.0.0.1', '{"parent_id":"8","title":"Configuration","icon":"fa-bars","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:48:25', '2018-02-21 01:48:25'),
(1013, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:48:26', '2018-02-21 01:48:26'),
(1014, 1, 'admin/auth/menu/9/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:48:32', '2018-02-21 01:48:32'),
(1015, 1, 'admin/auth/menu/9', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Currency","icon":"fa-dollar","uri":"auth\\/currency","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:48:37', '2018-02-21 01:48:37'),
(1016, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:48:37', '2018-02-21 01:48:37'),
(1017, 1, 'admin/auth/menu/10/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:48:41', '2018-02-21 01:48:41'),
(1018, 1, 'admin/auth/menu/10', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Price List","icon":"fa-list","uri":"auth\\/pricelists","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:48:46', '2018-02-21 01:48:46'),
(1019, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:48:47', '2018-02-21 01:48:47'),
(1020, 1, 'admin/auth/menu/11/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:48:52', '2018-02-21 01:48:52'),
(1021, 1, 'admin/auth/menu/11', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Tax","icon":"fa-percent","uri":"auth\\/tax","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:48:57', '2018-02-21 01:48:57'),
(1022, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:48:57', '2018-02-21 01:48:57'),
(1023, 1, 'admin/auth/menu/12/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:49:05', '2018-02-21 01:49:05'),
(1024, 1, 'admin/auth/menu/12', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Payment Terms","icon":"fa-align-justify","uri":"auth\\/paymentterms","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:49:10', '2018-02-21 01:49:10'),
(1025, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:49:10', '2018-02-21 01:49:10'),
(1026, 1, 'admin/auth/menu/13/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:49:18', '2018-02-21 01:49:18'),
(1027, 1, 'admin/auth/menu/13', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Payment Methods","icon":"fa-bars","uri":"auth\\/paymentmethods","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:49:22', '2018-02-21 01:49:22'),
(1028, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:49:23', '2018-02-21 01:49:23'),
(1029, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:49:28', '2018-02-21 01:49:28'),
(1030, 1, 'admin/auth/menu/14', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Stock Adj. Reasons","icon":"fa-bars","uri":"auth\\/stockadjustmentreasons","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:49:33', '2018-02-21 01:49:33'),
(1031, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:49:33', '2018-02-21 01:49:33'),
(1032, 1, 'admin/auth/menu/18/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:49:39', '2018-02-21 01:49:39'),
(1033, 1, 'admin/auth/menu/18', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Suppliers","icon":"fa-bars","uri":"auth\\/suppliers","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:49:43', '2018-02-21 01:49:43'),
(1034, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:49:44', '2018-02-21 01:49:44'),
(1035, 1, 'admin/auth/menu/17/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:49:50', '2018-02-21 01:49:50'),
(1036, 1, 'admin/auth/menu/17', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Warehouse","icon":"fa-stop-circle","uri":"auth\\/warehouse","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:49:55', '2018-02-21 01:49:55'),
(1037, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:49:55', '2018-02-21 01:49:55'),
(1038, 1, 'admin/auth/menu/16/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:02', '2018-02-21 01:50:02'),
(1039, 1, 'admin/auth/menu/16', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Company","icon":"fa-home","uri":"auth\\/company","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:50:06', '2018-02-21 01:50:06'),
(1040, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:50:07', '2018-02-21 01:50:07'),
(1041, 1, 'admin/auth/menu/19/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:12', '2018-02-21 01:50:12'),
(1042, 1, 'admin/auth/menu/19', 'PUT', '127.0.0.1', '{"parent_id":"8","title":"Product Configuration","icon":"fa-bars","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:50:17', '2018-02-21 01:50:17'),
(1043, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:50:17', '2018-02-21 01:50:17'),
(1044, 1, 'admin/auth/menu/23/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:22', '2018-02-21 01:50:22'),
(1045, 1, 'admin/auth/menu/23', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Products","icon":"fa-bars","uri":"auth\\/products","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:50:26', '2018-02-21 01:50:26'),
(1046, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:50:26', '2018-02-21 01:50:26'),
(1047, 1, 'admin/auth/menu/23/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:40', '2018-02-21 01:50:40'),
(1048, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:43', '2018-02-21 01:50:43'),
(1049, 1, 'admin/auth/menu/23/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:49', '2018-02-21 01:50:49'),
(1050, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:52', '2018-02-21 01:50:52'),
(1051, 1, 'admin/auth/menu/22/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:50:57', '2018-02-21 01:50:57'),
(1052, 1, 'admin/auth/menu/22', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Category","icon":"fa-bars","uri":"auth\\/category","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:51:01', '2018-02-21 01:51:01'),
(1053, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:51:01', '2018-02-21 01:51:01'),
(1054, 1, 'admin/auth/menu/21/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:06', '2018-02-21 01:51:06'),
(1055, 1, 'admin/auth/menu/21', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Brands","icon":"fa-bars","uri":"auth\\/brands","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:51:11', '2018-02-21 01:51:11'),
(1056, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:51:12', '2018-02-21 01:51:12'),
(1057, 1, 'admin/auth/menu/20/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:15', '2018-02-21 01:51:15'),
(1058, 1, 'admin/auth/menu/20', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Product Types","icon":"fa-bars","uri":"auth\\/producttypes","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:51:21', '2018-02-21 01:51:21'),
(1059, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:51:21', '2018-02-21 01:51:21'),
(1060, 1, 'admin/auth/menu/27/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:25', '2018-02-21 01:51:25'),
(1061, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:28', '2018-02-21 01:51:28'),
(1062, 1, 'admin/auth/menu/29/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:32', '2018-02-21 01:51:32'),
(1063, 1, 'admin/auth/menu/29', 'PUT', '127.0.0.1', '{"parent_id":"27","title":"Stock","icon":"fa-bars","uri":"auth\\/stock","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:51:38', '2018-02-21 01:51:38'),
(1064, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:51:38', '2018-02-21 01:51:38'),
(1065, 1, 'admin/auth/menu/28/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:51:48', '2018-02-21 01:51:48'),
(1066, 1, 'admin/auth/menu/28', 'PUT', '127.0.0.1', '{"parent_id":"27","title":"Bin Card","icon":"fa-bars","uri":"auth\\/bincard","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:51:58', '2018-02-21 01:51:58'),
(1067, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:51:59', '2018-02-21 01:51:59'),
(1068, 1, 'admin/auth/menu/26/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:52:03', '2018-02-21 01:52:03'),
(1069, 1, 'admin/auth/menu/26', 'PUT', '127.0.0.1', '{"parent_id":"24","title":"Stock Transfer","icon":"fa-bars","uri":"auth\\/stock_transfer","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:52:08', '2018-02-21 01:52:08'),
(1070, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:52:08', '2018-02-21 01:52:08'),
(1071, 1, 'admin/auth/menu/25/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:52:12', '2018-02-21 01:52:12'),
(1072, 1, 'admin/auth/menu/25', 'PUT', '127.0.0.1', '{"parent_id":"24","title":"Manage GRN","icon":"fa-bars","uri":"auth\\/grn","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:52:17', '2018-02-21 01:52:17'),
(1073, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:52:17', '2018-02-21 01:52:17'),
(1074, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:52:23', '2018-02-21 01:52:23'),
(1075, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:52:29', '2018-02-21 01:52:29'),
(1076, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:52:49', '2018-02-21 01:52:49'),
(1077, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:52:55', '2018-02-21 01:52:55'),
(1078, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:01', '2018-02-21 01:53:01'),
(1079, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:05', '2018-02-21 01:53:05'),
(1080, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:08', '2018-02-21 01:53:08'),
(1081, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:17', '2018-02-21 01:53:17'),
(1082, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:19', '2018-02-21 01:53:19'),
(1083, 1, 'admin/auth/menu/2', 'PUT', '127.0.0.1', '{"parent_id":"0","title":"Admin","icon":"fa-tasks","uri":null,"roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:53:23', '2018-02-21 01:53:23'),
(1084, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:53:24', '2018-02-21 01:53:24'),
(1085, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:29', '2018-02-21 01:53:29'),
(1086, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:32', '2018-02-21 01:53:32'),
(1087, 1, 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:35', '2018-02-21 01:53:35'),
(1088, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:37', '2018-02-21 01:53:37'),
(1089, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:40', '2018-02-21 01:53:40'),
(1090, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:53:46', '2018-02-21 01:53:46'),
(1091, 2, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:50', '2018-02-21 01:53:50'),
(1092, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:52', '2018-02-21 01:53:52'),
(1093, 2, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:55', '2018-02-21 01:53:55'),
(1094, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:57', '2018-02-21 01:53:57'),
(1095, 2, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:53:59', '2018-02-21 01:53:59'),
(1096, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:00', '2018-02-21 01:54:00'),
(1097, 2, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:01', '2018-02-21 01:54:01'),
(1098, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:07', '2018-02-21 01:54:07'),
(1099, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:54:12', '2018-02-21 01:54:12'),
(1100, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:16', '2018-02-21 01:54:16'),
(1101, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:18', '2018-02-21 01:54:18'),
(1102, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:21', '2018-02-21 01:54:21'),
(1103, 1, 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Users","icon":"fa-users","uri":"auth\\/users","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:54:30', '2018-02-21 01:54:30'),
(1104, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:54:30', '2018-02-21 01:54:30'),
(1105, 1, 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:34', '2018-02-21 01:54:34'),
(1106, 1, 'admin/auth/menu/4', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Roles","icon":"fa-user","uri":"auth\\/roles","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:54:37', '2018-02-21 01:54:37'),
(1107, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:54:37', '2018-02-21 01:54:37'),
(1108, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:40', '2018-02-21 01:54:40'),
(1109, 1, 'admin/auth/menu/5', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Permission","icon":"fa-ban","uri":"auth\\/permissions","roles":["2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:54:43', '2018-02-21 01:54:43'),
(1110, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:54:43', '2018-02-21 01:54:43'),
(1111, 1, 'admin/auth/menu/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:45', '2018-02-21 01:54:45'),
(1112, 1, 'admin/auth/menu/6', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Menu","icon":"fa-bars","uri":"auth\\/menu","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:54:52', '2018-02-21 01:54:52'),
(1113, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:54:52', '2018-02-21 01:54:52'),
(1114, 1, 'admin/auth/menu/7/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:54:56', '2018-02-21 01:54:56'),
(1115, 1, 'admin/auth/menu/7', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Operation log","icon":"fa-history","uri":"auth\\/logs","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:55:00', '2018-02-21 01:55:00'),
(1116, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:55:01', '2018-02-21 01:55:01'),
(1117, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:06', '2018-02-21 01:55:06'),
(1118, 1, 'admin/auth/menu/5', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Permission","icon":"fa-ban","uri":"auth\\/permissions","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:55:19', '2018-02-21 01:55:19'),
(1119, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:55:20', '2018-02-21 01:55:20'),
(1120, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:24', '2018-02-21 01:55:24'),
(1121, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:28', '2018-02-21 01:55:28'),
(1122, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:31', '2018-02-21 01:55:31'),
(1123, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:37', '2018-02-21 01:55:37'),
(1124, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:40', '2018-02-21 01:55:40'),
(1125, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:44', '2018-02-21 01:55:44'),
(1126, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:47', '2018-02-21 01:55:47'),
(1127, 1, 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Users","icon":"fa-users","uri":"auth\\/users","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:55:52', '2018-02-21 01:55:52'),
(1128, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:55:52', '2018-02-21 01:55:52'),
(1129, 1, 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:55:55', '2018-02-21 01:55:55'),
(1130, 1, 'admin/auth/menu/4', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Roles","icon":"fa-user","uri":"auth\\/roles","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:56:00', '2018-02-21 01:56:00'),
(1131, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:56:00', '2018-02-21 01:56:00'),
(1132, 1, 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:03', '2018-02-21 01:56:03'),
(1133, 1, 'admin/auth/menu/5', 'PUT', '127.0.0.1', '{"parent_id":"2","title":"Permission","icon":"fa-ban","uri":"auth\\/permissions","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 01:56:10', '2018-02-21 01:56:10'),
(1134, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 01:56:10', '2018-02-21 01:56:10'),
(1135, 1, 'admin/auth/menu/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:14', '2018-02-21 01:56:14'),
(1136, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:17', '2018-02-21 01:56:17'),
(1137, 1, 'admin/auth/menu/7/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:19', '2018-02-21 01:56:19'),
(1138, 1, 'admin/auth/menu/7/edit', 'GET', '127.0.0.1', '[]', '2018-02-21 01:56:25', '2018-02-21 01:56:25'),
(1139, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:28', '2018-02-21 01:56:28'),
(1140, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:30', '2018-02-21 01:56:30'),
(1141, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:31', '2018-02-21 01:56:31'),
(1142, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:32', '2018-02-21 01:56:32'),
(1143, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:37', '2018-02-21 01:56:37'),
(1144, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:56:43', '2018-02-21 01:56:43'),
(1145, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:46', '2018-02-21 01:56:46'),
(1146, 2, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:48', '2018-02-21 01:56:48'),
(1147, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:50', '2018-02-21 01:56:50'),
(1148, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:53', '2018-02-21 01:56:53'),
(1149, 2, 'admin/auth/currency', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:56:59', '2018-02-21 01:56:59'),
(1150, 2, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:00', '2018-02-21 01:57:00'),
(1151, 2, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:01', '2018-02-21 01:57:01'),
(1152, 2, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:02', '2018-02-21 01:57:02'),
(1153, 2, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:03', '2018-02-21 01:57:03'),
(1154, 2, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:04', '2018-02-21 01:57:04'),
(1155, 2, 'admin/auth/suppliers', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:05', '2018-02-21 01:57:05'),
(1156, 2, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:06', '2018-02-21 01:57:06'),
(1157, 2, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:08', '2018-02-21 01:57:08'),
(1158, 2, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:10', '2018-02-21 01:57:10'),
(1159, 2, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:14', '2018-02-21 01:57:14'),
(1160, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:17', '2018-02-21 01:57:17'),
(1161, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:18', '2018-02-21 01:57:18'),
(1162, 2, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:19', '2018-02-21 01:57:19'),
(1163, 2, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:20', '2018-02-21 01:57:20'),
(1164, 2, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:22', '2018-02-21 01:57:22'),
(1165, 2, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:25', '2018-02-21 01:57:25'),
(1166, 2, 'admin/auth/bincard', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:26', '2018-02-21 01:57:26'),
(1167, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:29', '2018-02-21 01:57:29'),
(1168, 2, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:31', '2018-02-21 01:57:31'),
(1169, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:45', '2018-02-21 01:57:45'),
(1170, 2, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:49', '2018-02-21 01:57:49'),
(1171, 2, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:50', '2018-02-21 01:57:50'),
(1172, 2, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:53', '2018-02-21 01:57:53'),
(1173, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:57:58', '2018-02-21 01:57:58'),
(1174, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 01:58:08', '2018-02-21 01:58:08'),
(1175, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:11', '2018-02-21 01:58:11'),
(1176, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:13', '2018-02-21 01:58:13'),
(1177, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:18', '2018-02-21 01:58:18'),
(1178, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:25', '2018-02-21 01:58:25'),
(1179, 1, 'admin/auth/menu/19/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:31', '2018-02-21 01:58:31'),
(1180, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:35', '2018-02-21 01:58:35'),
(1181, 1, 'admin/auth/menu/23/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:39', '2018-02-21 01:58:39'),
(1182, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:58:55', '2018-02-21 01:58:55'),
(1183, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:59:10', '2018-02-21 01:59:10'),
(1184, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{"slug":"reviva.management","name":"Reviva Management","http_method":[null],"http_path":"\\/auth\\/users\\r\\n\\/auth\\/roles\\r\\n\\/auth\\/permissions","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 01:59:23', '2018-02-21 01:59:23'),
(1185, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 01:59:23', '2018-02-21 01:59:23'),
(1186, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 01:59:39', '2018-02-21 01:59:39'),
(1187, 1, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:00:20', '2018-02-21 02:00:20'),
(1188, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:00:28', '2018-02-21 02:00:28'),
(1189, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:00:30', '2018-02-21 02:00:30'),
(1190, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{"slug":"auth.product.configuration","name":"Auth Product Configuration","http_method":[null],"http_path":"\\/auth\\/products\\r\\n\\/auth\\/category\\r\\n\\/auth\\/brands\\r\\n\\/auth\\/producttypes","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:01:11', '2018-02-21 02:01:11'),
(1191, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:01:12', '2018-02-21 02:01:12'),
(1192, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:01:18', '2018-02-21 02:01:18'),
(1193, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:01:20', '2018-02-21 02:01:20'),
(1194, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-21 02:02:04', '2018-02-21 02:02:04'),
(1195, 1, 'admin/auth/roles/2', 'PUT', '127.0.0.1', '{"slug":"reviva.admin","name":"Reviva Administrator","permissions":["2","3","4","6","7","8","9","10",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT"}', '2018-02-21 02:02:12', '2018-02-21 02:02:12'),
(1196, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2018-02-21 02:02:12', '2018-02-21 02:02:12'),
(1197, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:17', '2018-02-21 02:02:17'),
(1198, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:02:22', '2018-02-21 02:02:22'),
(1199, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:25', '2018-02-21 02:02:25'),
(1200, 2, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:27', '2018-02-21 02:02:27'),
(1201, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:28', '2018-02-21 02:02:28'),
(1202, 2, 'admin/auth/currency', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:32', '2018-02-21 02:02:32'),
(1203, 2, 'admin/auth/pricelists', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:32', '2018-02-21 02:02:32'),
(1204, 2, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:33', '2018-02-21 02:02:33'),
(1205, 2, 'admin/auth/paymentterms', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:34', '2018-02-21 02:02:34'),
(1206, 2, 'admin/auth/paymentmethods', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:35', '2018-02-21 02:02:35'),
(1207, 2, 'admin/auth/stockadjustmentreasons', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:36', '2018-02-21 02:02:36'),
(1208, 2, 'admin/auth/suppliers', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:36', '2018-02-21 02:02:36'),
(1209, 2, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:37', '2018-02-21 02:02:37'),
(1210, 2, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:38', '2018-02-21 02:02:38'),
(1211, 2, 'admin/auth/warehouse', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:39', '2018-02-21 02:02:39'),
(1212, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:43', '2018-02-21 02:02:43'),
(1213, 2, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:45', '2018-02-21 02:02:45'),
(1214, 2, 'admin/auth/brands', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:46', '2018-02-21 02:02:46'),
(1215, 2, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:47', '2018-02-21 02:02:47'),
(1216, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:02:53', '2018-02-21 02:02:53'),
(1217, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:02:59', '2018-02-21 02:02:59'),
(1218, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:03:03', '2018-02-21 02:03:03'),
(1219, 1, 'admin/auth/permissions/7/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:03:09', '2018-02-21 02:03:09'),
(1220, 1, 'admin/auth/permissions/7', 'PUT', '127.0.0.1', '{"slug":"auth.configuration","name":"Auth Configuration","http_method":[null],"http_path":"\\/auth\\/currency\\r\\n\\/auth\\/pricelists\\r\\n\\/auth\\/tax\\r\\n\\/auth\\/paymentterms\\r\\n\\/auth\\/paymentmethods\\r\\n\\/auth\\/stockadjustmentreasons\\r\\n\\/auth\\/suppliers\\r\\n\\/auth\\/warehouse\\r\\n\\/auth\\/company","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:03:30', '2018-02-21 02:03:30'),
(1221, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:03:30', '2018-02-21 02:03:30'),
(1222, 1, 'admin/auth/tax', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:03:47', '2018-02-21 02:03:47'),
(1223, 1, 'admin/auth/tax/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:03:51', '2018-02-21 02:03:51'),
(1224, 1, 'admin/auth/tax/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-21 02:04:48', '2018-02-21 02:04:48'),
(1225, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:05:48', '2018-02-21 02:05:48'),
(1226, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:06:00', '2018-02-21 02:06:00'),
(1227, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:06:01', '2018-02-21 02:06:01'),
(1228, 1, 'admin/auth/menu/16/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:06:08', '2018-02-21 02:06:08'),
(1229, 1, 'admin/auth/menu/16', 'PUT', '127.0.0.1', '{"parent_id":"15","title":"Customer","icon":"fa-home","uri":"auth\\/company","roles":["1","2",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 02:06:20', '2018-02-21 02:06:20'),
(1230, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:06:21', '2018-02-21 02:06:21'),
(1231, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:06:24', '2018-02-21 02:06:24'),
(1232, 1, 'admin/auth/company', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:06:29', '2018-02-21 02:06:29'),
(1233, 1, 'admin/auth/category', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:07:00', '2018-02-21 02:07:00'),
(1234, 1, 'admin/auth/category/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:07:05', '2018-02-21 02:07:05'),
(1235, 1, 'admin/auth/category/2/edit', 'GET', '127.0.0.1', '[]', '2018-02-21 02:07:24', '2018-02-21 02:07:24'),
(1236, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:08:31', '2018-02-21 02:08:31'),
(1237, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:08:34', '2018-02-21 02:08:34'),
(1238, 1, 'admin/auth/menu/22/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:08:52', '2018-02-21 02:08:52'),
(1239, 1, 'admin/auth/menu/22', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Category","icon":"fa-bars","uri":"auth\\/category","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 02:09:01', '2018-02-21 02:09:01'),
(1240, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:09:01', '2018-02-21 02:09:01'),
(1241, 1, 'admin/auth/menu/21/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:09:07', '2018-02-21 02:09:07'),
(1242, 1, 'admin/auth/menu/21', 'PUT', '127.0.0.1', '{"parent_id":"19","title":"Brands","icon":"fa-bars","uri":"auth\\/brands","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 02:09:11', '2018-02-21 02:09:11'),
(1243, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:09:11', '2018-02-21 02:09:11'),
(1244, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:09:15', '2018-02-21 02:09:15'),
(1245, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:09:25', '2018-02-21 02:09:25'),
(1246, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:09:30', '2018-02-21 02:09:30'),
(1247, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:11:21', '2018-02-21 02:11:21'),
(1248, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:27', '2018-02-21 02:11:27'),
(1249, 2, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:29', '2018-02-21 02:11:29'),
(1250, 2, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:32', '2018-02-21 02:11:32'),
(1251, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:33', '2018-02-21 02:11:33'),
(1252, 2, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:36', '2018-02-21 02:11:36'),
(1253, 2, 'admin/auth/producttypes', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:38', '2018-02-21 02:11:38'),
(1254, 2, 'admin/auth/producttypes/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:40', '2018-02-21 02:11:40'),
(1255, 2, 'admin/auth/currency', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:50', '2018-02-21 02:11:50'),
(1256, 2, 'admin/auth/currency/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:52', '2018-02-21 02:11:52'),
(1257, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:11:58', '2018-02-21 02:11:58'),
(1258, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:12:04', '2018-02-21 02:12:04'),
(1259, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:12:08', '2018-02-21 02:12:08'),
(1260, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:12:28', '2018-02-21 02:12:28'),
(1261, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:12:35', '2018-02-21 02:12:35'),
(1262, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:07', '2018-02-21 02:13:07'),
(1263, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:12', '2018-02-21 02:13:12'),
(1264, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:15', '2018-02-21 02:13:15'),
(1265, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:36', '2018-02-21 02:13:36'),
(1266, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:43', '2018-02-21 02:13:43'),
(1267, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{"slug":"reviva.management","name":"Reviva Management","http_method":[null],"http_path":"\\/auth\\/users\\r\\nauth\\/users\\/create\\r\\n\\/auth\\/roles\\r\\n\\/auth\\/permissions","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:13:48', '2018-02-21 02:13:48'),
(1268, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:13:48', '2018-02-21 02:13:48'),
(1269, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:13:53', '2018-02-21 02:13:53'),
(1270, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:13:58', '2018-02-21 02:13:58'),
(1271, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:01', '2018-02-21 02:14:01'),
(1272, 2, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:04', '2018-02-21 02:14:04'),
(1273, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:24', '2018-02-21 02:14:24'),
(1274, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:14:30', '2018-02-21 02:14:30'),
(1275, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:33', '2018-02-21 02:14:33'),
(1276, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:34', '2018-02-21 02:14:34'),
(1277, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:54', '2018-02-21 02:14:54'),
(1278, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:14:57', '2018-02-21 02:14:57'),
(1279, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:01', '2018-02-21 02:15:01'),
(1280, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{"slug":"reviva.management","name":"Reviva Management","http_method":[null],"http_path":"\\/auth\\/users*\\r\\n\\/auth\\/roles*\\r\\n\\/auth\\/permissions*","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:15:14', '2018-02-21 02:15:14'),
(1281, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:15:14', '2018-02-21 02:15:14'),
(1282, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:20', '2018-02-21 02:15:20'),
(1283, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:15:28', '2018-02-21 02:15:28'),
(1284, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:31', '2018-02-21 02:15:31'),
(1285, 2, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:33', '2018-02-21 02:15:33'),
(1286, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:36', '2018-02-21 02:15:36'),
(1287, 2, 'admin/auth/roles', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:39', '2018-02-21 02:15:39'),
(1288, 2, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:41', '2018-02-21 02:15:41'),
(1289, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:43', '2018-02-21 02:15:43'),
(1290, 2, 'admin/auth/permissions/7/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:15:52', '2018-02-21 02:15:52'),
(1291, 2, 'admin/auth/permissions/7', 'PUT', '127.0.0.1', '{"slug":"auth.configuration","name":"Auth Configuration","http_method":[null],"http_path":"\\/auth\\/currency*\\r\\n\\/auth\\/pricelists*\\r\\n\\/auth\\/tax*\\r\\n\\/auth\\/paymentterms*\\r\\n\\/auth\\/paymentmethods*\\r\\n\\/auth\\/stockadjustmentreasons*\\r\\n\\/auth\\/suppliers*\\r\\n\\/auth\\/warehouse*\\r\\n\\/auth\\/company*","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:16:18', '2018-02-21 02:16:18'),
(1292, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:16:18', '2018-02-21 02:16:18'),
(1293, 2, 'admin/auth/permissions/8/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:16:23', '2018-02-21 02:16:23'),
(1294, 2, 'admin/auth/permissions/8', 'PUT', '127.0.0.1', '{"slug":"auth.reports","name":"Auth Reports","http_method":[null],"http_path":"\\/auth\\/stock*\\r\\n\\/auth\\/bincard*","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:16:27', '2018-02-21 02:16:27'),
(1295, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:16:28', '2018-02-21 02:16:28'),
(1296, 2, 'admin/auth/permissions/10/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:16:31', '2018-02-21 02:16:31'),
(1297, 2, 'admin/auth/permissions/10', 'PUT', '127.0.0.1', '{"slug":"auth.product.configuration","name":"Auth Product Configuration","http_method":[null],"http_path":"\\/auth\\/products*\\r\\n\\/auth\\/category*\\r\\n\\/auth\\/brands*\\r\\n\\/auth\\/producttypes*","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/permissions"}', '2018-02-21 02:16:38', '2018-02-21 02:16:38'),
(1298, 2, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2018-02-21 02:16:38', '2018-02-21 02:16:38'),
(1299, 2, 'admin/auth/currency', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:16:45', '2018-02-21 02:16:45'),
(1300, 2, 'admin/auth/currency/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:16:47', '2018-02-21 02:16:47'),
(1301, 2, 'admin/auth/currency', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:16:49', '2018-02-21 02:16:49'),
(1302, 2, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:17:06', '2018-02-21 02:17:06'),
(1303, 2, 'admin/auth/products/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:17:09', '2018-02-21 02:17:09'),
(1304, 2, 'admin/auth/products', 'POST', '127.0.0.1', '{"product_name":"Product C","sku":"PC","description":"Product C","product_type_id":"1","supplier_id":"1","cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"150","status":"ACTIVE","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/products"}', '2018-02-21 02:17:36', '2018-02-21 02:17:36');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1305, 2, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-21 02:17:36', '2018-02-21 02:17:36'),
(1306, 2, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-21 02:17:58', '2018-02-21 02:17:58'),
(1307, 2, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-21 02:18:08', '2018-02-21 02:18:08'),
(1308, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:19:18', '2018-02-21 02:19:18'),
(1309, 2, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:19:22', '2018-02-21 02:19:22'),
(1310, 2, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '[]', '2018-02-21 02:19:46', '2018-02-21 02:19:46'),
(1311, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:19:50', '2018-02-21 02:19:50'),
(1312, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-21 02:21:52', '2018-02-21 02:21:52'),
(1313, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:22:01', '2018-02-21 02:22:01'),
(1314, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:22:03', '2018-02-21 02:22:03'),
(1315, 2, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '[]', '2018-02-21 02:22:20', '2018-02-21 02:22:20'),
(1316, 2, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:22:23', '2018-02-21 02:22:23'),
(1317, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:23:24', '2018-02-21 02:23:24'),
(1318, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 02:23:29', '2018-02-21 02:23:29'),
(1319, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:23:42', '2018-02-21 02:23:42'),
(1320, 1, 'admin/auth/menu/28/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:23:47', '2018-02-21 02:23:47'),
(1321, 1, 'admin/auth/menu/28', 'PUT', '127.0.0.1', '{"parent_id":"27","title":"Bin Card","icon":"fa-bars","uri":"auth\\/bincard","roles":["1",null],"_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/menu"}', '2018-02-21 02:23:52', '2018-02-21 02:23:52'),
(1322, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:23:52', '2018-02-21 02:23:52'),
(1323, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-02-21 02:24:18', '2018-02-21 02:24:18'),
(1324, 1, 'admin/auth/bincard', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:24:26', '2018-02-21 02:24:26'),
(1325, 1, 'admin/auth/stock', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 02:24:27', '2018-02-21 02:24:27'),
(1326, 1, 'admin/auth/stock/create', 'POST', '127.0.0.1', '{"warehouse":"1","_token":"r0NLZLc28uM65I2ieoFm5vtM8d6M9CBKR4VKRyXZ"}', '2018-02-21 02:24:32', '2018-02-21 02:24:32'),
(1327, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:24:23', '2018-02-21 16:24:23'),
(1328, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:24:29', '2018-02-21 16:24:29'),
(1329, 2, 'admin/auth/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:24:33', '2018-02-21 16:24:33'),
(1330, 2, 'admin/auth/users', 'POST', '127.0.0.1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"admin","password_confirmation":"admin","roles":[null],"permissions":["2","3","4","8","9","10",null],"_token":"mrdrM9TMyp2ebK9k4kSbQzIfU9o8X2Ek555AUeSR","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-02-21 16:26:02', '2018-02-21 16:26:02'),
(1331, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-02-21 16:26:08', '2018-02-21 16:26:08'),
(1332, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:26:17', '2018-02-21 16:26:17'),
(1333, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:26:24', '2018-02-21 16:26:24'),
(1334, 3, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:26:29', '2018-02-21 16:26:29'),
(1335, 3, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:26:31', '2018-02-21 16:26:31'),
(1336, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:26:58', '2018-02-21 16:26:58'),
(1337, 3, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:27:03', '2018-02-21 16:27:03'),
(1338, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:27:11', '2018-02-21 16:27:11'),
(1339, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:27:15', '2018-02-21 16:27:15'),
(1340, 1, 'admin/auth/users/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:27:19', '2018-02-21 16:27:19'),
(1341, 1, 'admin/auth/users/3', 'PUT', '127.0.0.1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","password_confirmation":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","roles":["2",null],"permissions":[null],"_token":"mrdrM9TMyp2ebK9k4kSbQzIfU9o8X2Ek555AUeSR","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-02-21 16:27:42', '2018-02-21 16:27:42'),
(1342, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-02-21 16:27:47', '2018-02-21 16:27:47'),
(1343, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:27:53', '2018-02-21 16:27:53'),
(1344, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:28:00', '2018-02-21 16:28:00'),
(1345, 3, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:28:07', '2018-02-21 16:28:07'),
(1346, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:28:13', '2018-02-21 16:28:13'),
(1347, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:29:23', '2018-02-21 16:29:23'),
(1348, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:30:48', '2018-02-21 16:30:48'),
(1349, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:30:51', '2018-02-21 16:30:51'),
(1350, 2, 'admin/auth/users/3/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:30:54', '2018-02-21 16:30:54'),
(1351, 2, 'admin/auth/users/3', 'PUT', '127.0.0.1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","password_confirmation":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","roles":[null],"permissions":["2","3","4","7","8","9","10",null],"_token":"mrdrM9TMyp2ebK9k4kSbQzIfU9o8X2Ek555AUeSR","_method":"PUT","_previous_":"http:\\/\\/local.invoice.lk\\/admin\\/auth\\/users"}', '2018-02-21 16:32:07', '2018-02-21 16:32:07'),
(1352, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-02-21 16:32:07', '2018-02-21 16:32:07'),
(1353, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:32:13', '2018-02-21 16:32:13'),
(1354, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:32:20', '2018-02-21 16:32:20'),
(1355, 3, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:32:31', '2018-02-21 16:32:31'),
(1356, 3, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:32:32', '2018-02-21 16:32:32'),
(1357, 3, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:32:32', '2018-02-21 16:32:32'),
(1358, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:35:51', '2018-02-21 16:35:51'),
(1359, 3, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:36:33', '2018-02-21 16:36:33'),
(1360, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:36:53', '2018-02-21 16:36:53'),
(1361, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:36:59', '2018-02-21 16:36:59'),
(1362, 2, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:40:08', '2018-02-21 16:40:08'),
(1363, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 16:40:47', '2018-02-21 16:40:47'),
(1364, 3, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 16:41:40', '2018-02-21 16:41:40'),
(1365, 3, 'admin/auth/logout', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 17:21:29', '2018-02-21 17:21:29'),
(1366, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-21 17:22:55', '2018-02-21 17:22:55'),
(1367, 1, 'admin/auth/grn', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 17:23:02', '2018-02-21 17:23:02'),
(1368, 1, 'admin/auth/grn/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 17:23:05', '2018-02-21 17:23:05'),
(1369, 1, 'admin/auth/stock_transfer', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 17:23:21', '2018-02-21 17:23:21'),
(1370, 1, 'admin/auth/stock_transfer/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-21 17:23:24', '2018-02-21 17:23:24'),
(1371, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-02-23 01:41:29', '2018-02-23 01:41:29'),
(1372, 1, 'admin/auth/products', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-23 01:45:08', '2018-02-23 01:45:08'),
(1373, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-23 01:50:51', '2018-02-23 01:50:51'),
(1374, 1, 'admin/auth/products/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-02-23 01:50:55', '2018-02-23 01:50:55'),
(1375, 1, 'admin/auth/products/1', 'PUT', '127.0.0.1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"yEO96cCqTN505m1gOxd6izE6Efb7jZx5aY737GQB","_method":"PUT","_previous_":"http:\\/\\/local.kandula.lk\\/admin\\/auth\\/products"}', '2018-02-23 01:51:17', '2018-02-23 01:51:17'),
(1376, 1, 'admin/auth/products/1/edit', 'GET', '127.0.0.1', '[]', '2018-02-23 01:51:18', '2018-02-23 01:51:18'),
(1377, 1, 'admin/auth/products/1', 'PUT', '127.0.0.1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"yEO96cCqTN505m1gOxd6izE6Efb7jZx5aY737GQB","_method":"PUT"}', '2018-02-23 01:52:04', '2018-02-23 01:52:04'),
(1378, 1, 'admin/auth/products', 'GET', '127.0.0.1', '[]', '2018-02-23 01:52:07', '2018-02-23 01:52:07'),
(1379, 1, 'admin', 'GET', '::1', '[]', '2018-02-27 23:57:43', '2018-02-27 23:57:43'),
(1380, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 00:07:32', '2018-02-28 00:07:32'),
(1381, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:07:46', '2018-02-28 00:07:46'),
(1382, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:07:54', '2018-02-28 00:07:54'),
(1383, 1, 'admin/auth/products/1', 'GET', '::1', '[]', '2018-02-28 00:08:32', '2018-02-28 00:08:32'),
(1384, 1, 'admin/auth/brands', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:03', '2018-02-28 00:09:03'),
(1385, 1, 'admin/auth/brands/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:06', '2018-02-28 00:09:06'),
(1386, 1, 'admin/auth/brands/1', 'PUT', '::1', '{"type":"Brand1","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/brands"}', '2018-02-28 00:09:09', '2018-02-28 00:09:09'),
(1387, 1, 'admin/auth/brands', 'GET', '::1', '[]', '2018-02-28 00:09:10', '2018-02-28 00:09:10'),
(1388, 1, 'admin/auth/brands/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:15', '2018-02-28 00:09:15'),
(1389, 1, 'admin/auth/brands/1', 'PUT', '::1', '{"type":"Brand","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/brands"}', '2018-02-28 00:09:19', '2018-02-28 00:09:19'),
(1390, 1, 'admin/auth/brands', 'GET', '::1', '[]', '2018-02-28 00:09:20', '2018-02-28 00:09:20'),
(1391, 1, 'admin/auth/category', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:40', '2018-02-28 00:09:40'),
(1392, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:42', '2018-02-28 00:09:42'),
(1393, 1, 'admin/auth/producttypes', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:43', '2018-02-28 00:09:43'),
(1394, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:44', '2018-02-28 00:09:44'),
(1395, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:09:46', '2018-02-28 00:09:46'),
(1396, 1, 'admin/auth/products/1', 'GET', '::1', '[]', '2018-02-28 00:10:16', '2018-02-28 00:10:16'),
(1397, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 00:12:34', '2018-02-28 00:12:34'),
(1398, 1, 'admin/auth/grn', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:12:42', '2018-02-28 00:12:42'),
(1399, 1, 'admin/auth/grn/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:12:45', '2018-02-28 00:12:45'),
(1400, 1, 'admin/auth/grn/create', 'POST', '::1', '{"grn_number":"GR4","description":"Test","fields":[{"product_name":"1","qty":"10"}],"_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA"}', '2018-02-28 00:12:56', '2018-02-28 00:12:56'),
(1401, 1, 'admin/auth/grn/create', 'GET', '::1', '[]', '2018-02-28 00:12:57', '2018-02-28 00:12:57'),
(1402, 1, 'admin/auth/grn', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:13:03', '2018-02-28 00:13:03'),
(1403, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:13:13', '2018-02-28 00:13:13'),
(1404, 1, 'admin/auth/products/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:13:16', '2018-02-28 00:13:16'),
(1405, 1, 'admin/auth/products', 'POST', '::1', '{"product_name":"Test Product","sku":"TP","description":"Test","product_type_id":"2","supplier_id":"1","brand_id":"1","cost":"100","retail_price":"150","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:13:49', '2018-02-28 00:13:49'),
(1406, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:13:49', '2018-02-28 00:13:49'),
(1407, 1, 'admin/auth/products/4/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:13:53', '2018-02-28 00:13:53'),
(1408, 1, 'admin/auth/products/4', 'PUT', '::1', '{"product_name":"Test Product","sku":"TP","description":"Test","product_type_id":"2","supplier_id":"1","brand_id":"1","cost":"100","retail_price":"150","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:14:02', '2018-02-28 00:14:02'),
(1409, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:14:03', '2018-02-28 00:14:03'),
(1410, 1, 'admin/auth/products/4/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:14:12', '2018-02-28 00:14:12'),
(1411, 1, 'admin/auth/products/4', 'PUT', '::1', '{"product_name":"Test Product","sku":"TP","description":"Test","product_type_id":"2","supplier_id":"1","brand_id":"1","cost":"100","retail_price":"150","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:14:26', '2018-02-28 00:14:26'),
(1412, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:14:26', '2018-02-28 00:14:26'),
(1413, 1, 'admin/auth/products/4/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:15:45', '2018-02-28 00:15:45'),
(1414, 1, 'admin/auth/products/4', 'PUT', '::1', '{"product_name":"Test Product","sku":"TP","description":"Test","product_type_id":"2","supplier_id":"1","brand_id":"1","cost":"100","retail_price":"150","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:16:00', '2018-02-28 00:16:00'),
(1415, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:16:01', '2018-02-28 00:16:01'),
(1416, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:18:21', '2018-02-28 00:18:21'),
(1417, 1, 'admin/auth/users/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:18:25', '2018-02-28 00:18:25'),
(1418, 1, 'admin/auth/users/1', 'PUT', '::1', '{"username":"admin","first_name":"iNat","last_name":"Administrator","email":"arangaw@gmail.com","telephone":"0718068183","password":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","password_confirmation":"$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma","roles":["1",null],"permissions":[null],"_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/users"}', '2018-02-28 00:18:35', '2018-02-28 00:18:35'),
(1419, 1, 'admin/auth/users', 'GET', '::1', '[]', '2018-02-28 00:18:35', '2018-02-28 00:18:35'),
(1420, 1, 'admin/auth/users/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:18:39', '2018-02-28 00:18:39'),
(1421, 1, 'admin/auth/users/1/edit', 'GET', '::1', '[]', '2018-02-28 00:24:52', '2018-02-28 00:24:52'),
(1422, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:25:16', '2018-02-28 00:25:16'),
(1423, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:25:20', '2018-02-28 00:25:20'),
(1424, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:26:26', '2018-02-28 00:26:26'),
(1425, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 00:26:27', '2018-02-28 00:26:27'),
(1426, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT"}', '2018-02-28 00:26:39', '2018-02-28 00:26:39'),
(1427, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:26:39', '2018-02-28 00:26:39'),
(1428, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:26:43', '2018-02-28 00:26:43'),
(1429, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:26:54', '2018-02-28 00:26:54'),
(1430, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:26:55', '2018-02-28 00:26:55'),
(1431, 1, 'admin/auth/products/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:27:00', '2018-02-28 00:27:00'),
(1432, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:27:03', '2018-02-28 00:27:03'),
(1433, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:27:06', '2018-02-28 00:27:06'),
(1434, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 00:27:23', '2018-02-28 00:27:23'),
(1435, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 00:27:26', '2018-02-28 00:27:26'),
(1436, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT"}', '2018-02-28 00:39:42', '2018-02-28 00:39:42'),
(1437, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:39:43', '2018-02-28 00:39:43'),
(1438, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:39:56', '2018-02-28 00:39:56'),
(1439, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:40:00', '2018-02-28 00:40:00'),
(1440, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:40:08', '2018-02-28 00:40:08'),
(1441, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:40:11', '2018-02-28 00:40:11'),
(1442, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:40:32', '2018-02-28 00:40:32'),
(1443, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:40:56', '2018-02-28 00:40:56'),
(1444, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:43:08', '2018-02-28 00:43:08'),
(1445, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 00:43:17', '2018-02-28 00:43:17'),
(1446, 1, 'admin/auth/products/4', 'DELETE', '::1', '{"_method":"delete","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA"}', '2018-02-28 00:44:17', '2018-02-28 00:44:17'),
(1447, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:17', '2018-02-28 00:44:17'),
(1448, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:25', '2018-02-28 00:44:25'),
(1449, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:27', '2018-02-28 00:44:27'),
(1450, 1, 'admin/auth/products/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:29', '2018-02-28 00:44:29'),
(1451, 1, 'admin/auth/stock_transfer', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:39', '2018-02-28 00:44:39'),
(1452, 1, 'admin/auth/grn', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:48', '2018-02-28 00:44:48'),
(1453, 1, 'admin/auth/stock_transfer', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:44:59', '2018-02-28 00:44:59'),
(1454, 1, 'admin/auth/stock_transfer/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:45:01', '2018-02-28 00:45:01'),
(1455, 1, 'admin/auth/grn', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:45:05', '2018-02-28 00:45:05'),
(1456, 1, 'admin/auth/grn/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:45:07', '2018-02-28 00:45:07'),
(1457, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 00:54:20', '2018-02-28 00:54:20'),
(1458, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:02:06', '2018-02-28 01:02:06'),
(1459, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:02:06', '2018-02-28 01:02:06'),
(1460, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:02:06', '2018-02-28 01:02:06'),
(1461, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:02:06', '2018-02-28 01:02:06'),
(1462, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:04:51', '2018-02-28 01:04:51'),
(1463, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 01:05:00', '2018-02-28 01:05:00'),
(1464, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:05:00', '2018-02-28 01:05:00'),
(1465, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:05:22', '2018-02-28 01:05:22'),
(1466, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:05:57', '2018-02-28 01:05:57'),
(1467, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:07:00', '2018-02-28 01:07:00'),
(1468, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:07:08', '2018-02-28 01:07:08'),
(1469, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:08:22', '2018-02-28 01:08:22'),
(1470, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:10:01', '2018-02-28 01:10:01'),
(1471, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:10:26', '2018-02-28 01:10:26'),
(1472, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:10:34', '2018-02-28 01:10:34'),
(1473, 1, 'admin/auth/products', 'GET', '::1', '{"id":null,"_pjax":"#pjax-container"}', '2018-02-28 01:12:37', '2018-02-28 01:12:37'),
(1474, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:53', '2018-02-28 01:12:53'),
(1475, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:55', '2018-02-28 01:12:55'),
(1476, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:56', '2018-02-28 01:12:56'),
(1477, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:56', '2018-02-28 01:12:56'),
(1478, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:56', '2018-02-28 01:12:56'),
(1479, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:12:58', '2018-02-28 01:12:58'),
(1480, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container","id":"2"}', '2018-02-28 01:13:10', '2018-02-28 01:13:10'),
(1481, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:13:15', '2018-02-28 01:13:15'),
(1482, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:15:08', '2018-02-28 01:15:08'),
(1483, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:16:31', '2018-02-28 01:16:31'),
(1484, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:19:11', '2018-02-28 01:19:11'),
(1485, 1, 'admin/auth/products/1', 'PUT', '::1', '{"product_name":"Product B","sku":"123","description":"Description","product_type_id":"1","supplier_id":null,"brand_id":null,"cost":"100","retail_price":"125","manage_stock_level":"YES","re_order_level":"100","category_id":"3","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT"}', '2018-02-28 01:19:15', '2018-02-28 01:19:15'),
(1486, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:19:16', '2018-02-28 01:19:16'),
(1487, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:19:46', '2018-02-28 01:19:46'),
(1488, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:20:04', '2018-02-28 01:20:04'),
(1489, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:20:23', '2018-02-28 01:20:23'),
(1490, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:24:37', '2018-02-28 01:24:37'),
(1491, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:24:54', '2018-02-28 01:24:54'),
(1492, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:25:08', '2018-02-28 01:25:08'),
(1493, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:27:08', '2018-02-28 01:27:08'),
(1494, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:27:14', '2018-02-28 01:27:14'),
(1495, 1, 'admin/auth/products/1/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:31:14', '2018-02-28 01:31:14'),
(1496, 1, 'admin/auth/products/1/edit', 'GET', '::1', '[]', '2018-02-28 01:39:49', '2018-02-28 01:39:49'),
(1497, 1, 'admin/auth/warehouse', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:40:13', '2018-02-28 01:40:13'),
(1498, 1, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:40:20', '2018-02-28 01:40:20'),
(1499, 1, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:13', '2018-02-28 01:50:13'),
(1500, 1, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:15', '2018-02-28 01:50:15'),
(1501, 1, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:17', '2018-02-28 01:50:17'),
(1502, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:27', '2018-02-28 01:50:27'),
(1503, 1, 'admin/auth/products/1', 'DELETE', '::1', '{"_method":"delete","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA"}', '2018-02-28 01:50:32', '2018-02-28 01:50:32'),
(1504, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:33', '2018-02-28 01:50:33'),
(1505, 1, 'admin/auth/products/2/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:50:53', '2018-02-28 01:50:53'),
(1506, 1, 'admin/auth/products/2', 'PUT', '::1', '{"product_name":"Product A","sku":"S","description":null,"product_type_id":"1","supplier_id":null,"brand_id":null,"cost":null,"retail_price":"110","manage_stock_level":"YES","re_order_level":"10","category_id":"2","status":"ACTIVE","_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/products"}', '2018-02-28 01:51:02', '2018-02-28 01:51:02'),
(1507, 1, 'admin/auth/products', 'GET', '::1', '[]', '2018-02-28 01:51:02', '2018-02-28 01:51:02'),
(1508, 1, 'admin/auth/products/2/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 01:52:24', '2018-02-28 01:52:24'),
(1509, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 03:00:18', '2018-02-28 03:00:18'),
(1510, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:00:28', '2018-02-28 03:00:28'),
(1511, 1, 'admin/auth/users/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:00:34', '2018-02-28 03:00:34'),
(1512, 1, 'admin/auth/users/3', 'PUT', '::1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","password_confirmation":"$2y$10$XM8nP\\/dnSf5R7GQ2wvtFsuqPLHagbmtKBHVfCx3Q7GTNIstqBSyFG","roles":[null],"permissions":["2","9",null],"_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/users"}', '2018-02-28 03:01:00', '2018-02-28 03:01:00'),
(1513, 1, 'admin/auth/users', 'GET', '::1', '[]', '2018-02-28 03:01:01', '2018-02-28 03:01:01'),
(1514, 1, 'admin/auth/users/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:10', '2018-02-28 03:01:10'),
(1515, 1, 'admin/auth/users/3', 'PUT', '::1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"1qaz@#2wsx","password_confirmation":"1qaz@#2wsx","roles":[null],"permissions":["2","9",null],"_token":"Gu4TEPSy6JGqgvSKeJc74aTic5qWvHi7aAQm7vCA","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/users"}', '2018-02-28 03:01:25', '2018-02-28 03:01:25'),
(1516, 1, 'admin/auth/users', 'GET', '::1', '[]', '2018-02-28 03:01:26', '2018-02-28 03:01:26'),
(1517, 1, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:35', '2018-02-28 03:01:35'),
(1518, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:01:44', '2018-02-28 03:01:44'),
(1519, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:47', '2018-02-28 03:01:47'),
(1520, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:48', '2018-02-28 03:01:48'),
(1521, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:49', '2018-02-28 03:01:49'),
(1522, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:49', '2018-02-28 03:01:49'),
(1523, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:01:56', '2018-02-28 03:01:56'),
(1524, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:00', '2018-02-28 03:02:00'),
(1525, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:02:03', '2018-02-28 03:02:03'),
(1526, 3, 'admin/auth/setting', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:08', '2018-02-28 03:02:08'),
(1527, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:15', '2018-02-28 03:02:15'),
(1528, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:18', '2018-02-28 03:02:18'),
(1529, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:22', '2018-02-28 03:02:22'),
(1530, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:02:25', '2018-02-28 03:02:25'),
(1531, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:02:35', '2018-02-28 03:02:35'),
(1532, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:03:10', '2018-02-28 03:03:10'),
(1533, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:03:13', '2018-02-28 03:03:13'),
(1534, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 03:04:18', '2018-02-28 03:04:18'),
(1535, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:04:26', '2018-02-28 03:04:26'),
(1536, 1, 'admin/auth/roles', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:04:35', '2018-02-28 03:04:35'),
(1537, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:04:46', '2018-02-28 03:04:46'),
(1538, 1, 'admin/auth/roles', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:04:49', '2018-02-28 03:04:49'),
(1539, 1, 'admin/auth/roles/create', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:04:51', '2018-02-28 03:04:51'),
(1540, 1, 'admin/auth/roles', 'POST', '::1', '{"slug":"test","name":"admin","permissions":["3","9",null],"_token":"RgjIrxk8fUIgR7rXy1vU1DnC7ZZ3HePWiu7c4o69","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/roles"}', '2018-02-28 03:05:15', '2018-02-28 03:05:15'),
(1541, 1, 'admin/auth/roles', 'GET', '::1', '[]', '2018-02-28 03:05:15', '2018-02-28 03:05:15'),
(1542, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:05:28', '2018-02-28 03:05:28'),
(1543, 1, 'admin/auth/users/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:05:31', '2018-02-28 03:05:31'),
(1544, 1, 'admin/auth/users/3', 'PUT', '::1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"$2y$10$ntu2S.WfkT2\\/ERYRpnuqo.Ijj9CMrKZj0OQUWDmKUiLay3fibIEaG","password_confirmation":"$2y$10$ntu2S.WfkT2\\/ERYRpnuqo.Ijj9CMrKZj0OQUWDmKUiLay3fibIEaG","roles":["3",null],"permissions":["2","3","9",null],"_token":"RgjIrxk8fUIgR7rXy1vU1DnC7ZZ3HePWiu7c4o69","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/users"}', '2018-02-28 03:06:42', '2018-02-28 03:06:42'),
(1545, 1, 'admin/auth/users', 'GET', '::1', '[]', '2018-02-28 03:06:42', '2018-02-28 03:06:42'),
(1546, 1, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:06:47', '2018-02-28 03:06:47'),
(1547, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:06:56', '2018-02-28 03:06:56'),
(1548, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:06:59', '2018-02-28 03:06:59'),
(1549, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:02', '2018-02-28 03:07:02'),
(1550, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 03:07:10', '2018-02-28 03:07:10'),
(1551, 1, 'admin/auth/permissions', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:14', '2018-02-28 03:07:14'),
(1552, 1, 'admin/auth/roles', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:44', '2018-02-28 03:07:44'),
(1553, 1, 'admin/auth/roles/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:46', '2018-02-28 03:07:46'),
(1554, 1, 'admin/auth/roles/3', 'PUT', '::1', '{"slug":"test","name":"admin","permissions":["3","9","10",null],"_token":"RgjIrxk8fUIgR7rXy1vU1DnC7ZZ3HePWiu7c4o69","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/roles"}', '2018-02-28 03:07:51', '2018-02-28 03:07:51'),
(1555, 1, 'admin/auth/roles', 'GET', '::1', '[]', '2018-02-28 03:07:51', '2018-02-28 03:07:51'),
(1556, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:55', '2018-02-28 03:07:55'),
(1557, 1, 'admin/auth/users/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:07:59', '2018-02-28 03:07:59'),
(1558, 1, 'admin/auth/users/3', 'PUT', '::1', '{"username":"user","first_name":"Test","last_name":"User","email":"test@admin.com","telephone":"0718068183","password":"$2y$10$ntu2S.WfkT2\\/ERYRpnuqo.Ijj9CMrKZj0OQUWDmKUiLay3fibIEaG","password_confirmation":"$2y$10$ntu2S.WfkT2\\/ERYRpnuqo.Ijj9CMrKZj0OQUWDmKUiLay3fibIEaG","roles":["3",null],"permissions":["2","3","10",null],"_token":"RgjIrxk8fUIgR7rXy1vU1DnC7ZZ3HePWiu7c4o69","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/users"}', '2018-02-28 03:08:19', '2018-02-28 03:08:19'),
(1559, 1, 'admin/auth/users', 'GET', '::1', '[]', '2018-02-28 03:08:20', '2018-02-28 03:08:20'),
(1560, 1, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:26', '2018-02-28 03:08:26'),
(1561, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:08:34', '2018-02-28 03:08:34'),
(1562, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:37', '2018-02-28 03:08:37'),
(1563, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:38', '2018-02-28 03:08:38'),
(1564, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:38', '2018-02-28 03:08:38'),
(1565, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:39', '2018-02-28 03:08:39'),
(1566, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:39', '2018-02-28 03:08:39'),
(1567, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:41', '2018-02-28 03:08:41'),
(1568, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:42', '2018-02-28 03:08:42'),
(1569, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:42', '2018-02-28 03:08:42'),
(1570, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:42', '2018-02-28 03:08:42'),
(1571, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:42', '2018-02-28 03:08:42'),
(1572, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:43', '2018-02-28 03:08:43'),
(1573, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:43', '2018-02-28 03:08:43'),
(1574, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:43', '2018-02-28 03:08:43'),
(1575, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:43', '2018-02-28 03:08:43'),
(1576, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:44', '2018-02-28 03:08:44'),
(1577, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:44', '2018-02-28 03:08:44'),
(1578, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:47', '2018-02-28 03:08:47'),
(1579, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 03:08:53', '2018-02-28 03:08:53'),
(1580, 1, 'admin/auth/users', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:08:57', '2018-02-28 03:08:57'),
(1581, 1, 'admin/auth/roles', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:07', '2018-02-28 03:09:07'),
(1582, 1, 'admin/auth/roles/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:25', '2018-02-28 03:09:25'),
(1583, 1, 'admin/auth/roles/3', 'PUT', '::1', '{"slug":"test","name":"admin","permissions":["2","3","9","10",null],"_token":"RgjIrxk8fUIgR7rXy1vU1DnC7ZZ3HePWiu7c4o69","_method":"PUT","_previous_":"http:\\/\\/kandula.local\\/admin\\/auth\\/roles"}', '2018-02-28 03:09:28', '2018-02-28 03:09:28'),
(1584, 1, 'admin/auth/roles', 'GET', '::1', '[]', '2018-02-28 03:09:28', '2018-02-28 03:09:28'),
(1585, 1, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:35', '2018-02-28 03:09:35'),
(1586, 3, 'admin', 'GET', '::1', '[]', '2018-02-28 03:09:44', '2018-02-28 03:09:44'),
(1587, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:46', '2018-02-28 03:09:46'),
(1588, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:46', '2018-02-28 03:09:46'),
(1589, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:46', '2018-02-28 03:09:46'),
(1590, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:46', '2018-02-28 03:09:46'),
(1591, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:47', '2018-02-28 03:09:47'),
(1592, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:47', '2018-02-28 03:09:47'),
(1593, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:47', '2018-02-28 03:09:47'),
(1594, 3, 'admin', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:47', '2018-02-28 03:09:47'),
(1595, 3, 'admin/auth/logout', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:09:54', '2018-02-28 03:09:54'),
(1596, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 03:10:02', '2018-02-28 03:10:02'),
(1597, 1, 'admin/auth/menu', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:10:06', '2018-02-28 03:10:06'),
(1598, 1, 'admin/auth/roles', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:10:13', '2018-02-28 03:10:13'),
(1599, 1, 'admin/auth/roles/3/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 03:10:33', '2018-02-28 03:10:33'),
(1600, 1, 'admin', 'GET', '::1', '[]', '2018-02-28 05:44:39', '2018-02-28 05:44:39'),
(1601, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 05:44:47', '2018-02-28 05:44:47'),
(1602, 1, 'admin/auth/products/2/edit', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-02-28 05:45:13', '2018-02-28 05:45:13'),
(1603, 1, 'admin/auth/products/2', 'GET', '::1', '[]', '2018-02-28 05:45:28', '2018-02-28 05:45:28'),
(1604, 1, 'admin', 'GET', '::1', '[]', '2018-03-05 00:26:44', '2018-03-05 00:26:44'),
(1605, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-03-05 00:26:53', '2018-03-05 00:26:53'),
(1606, 1, 'admin', 'GET', '::1', '[]', '2018-03-05 23:42:46', '2018-03-05 23:42:46'),
(1607, 1, 'admin', 'GET', '::1', '[]', '2018-03-16 03:07:11', '2018-03-16 03:07:11'),
(1608, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-03-16 03:07:21', '2018-03-16 03:07:21'),
(1609, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-03-16 03:07:24', '2018-03-16 03:07:24'),
(1610, 1, 'admin/auth/products', 'GET', '::1', '{"_pjax":"#pjax-container"}', '2018-03-16 03:07:25', '2018-03-16 03:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, 'Reviva Management', 'reviva.management', '', '/auth/users*\r\n/auth/roles*\r\n/auth/permissions*', '2018-02-21 01:24:11', '2018-02-21 02:15:14'),
(7, 'Auth Configuration', 'auth.configuration', '', '/auth/currency*\r\n/auth/pricelists*\r\n/auth/tax*\r\n/auth/paymentterms*\r\n/auth/paymentmethods*\r\n/auth/stockadjustmentreasons*\r\n/auth/suppliers*\r\n/auth/warehouse*\r\n/auth/company*', '2018-02-21 01:26:49', '2018-02-21 02:16:18'),
(8, 'Auth Reports', 'auth.reports', '', '/auth/stock*\r\n/auth/bincard*', '2018-02-21 01:27:27', '2018-02-21 02:16:27'),
(9, 'Auth Inventory', 'auth.inventory', '', '/auth/stock_transfer\r\n/auth/grn', '2018-02-21 01:28:03', '2018-02-21 01:28:03'),
(10, 'Auth Product Configuration', 'auth.product.configuration', '', '/auth/products*\r\n/auth/category*\r\n/auth/brands*\r\n/auth/producttypes*', '2018-02-21 02:01:11', '2018-02-21 02:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2018-01-31 05:13:52', '2018-01-31 05:13:52'),
(2, 'Reviva Administrator', 'reviva.admin', '2018-02-21 01:28:49', '2018-02-21 01:28:49'),
(3, 'admin', 'test', '2018-02-28 03:05:15', '2018-02-28 03:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 10, NULL, NULL),
(1, 11, NULL, NULL),
(1, 12, NULL, NULL),
(1, 13, NULL, NULL),
(1, 14, NULL, NULL),
(1, 15, NULL, NULL),
(1, 16, NULL, NULL),
(1, 17, NULL, NULL),
(1, 18, NULL, NULL),
(1, 19, NULL, NULL),
(1, 20, NULL, NULL),
(1, 21, NULL, NULL),
(1, 22, NULL, NULL),
(1, 23, NULL, NULL),
(1, 24, NULL, NULL),
(1, 25, NULL, NULL),
(1, 26, NULL, NULL),
(1, 27, NULL, NULL),
(1, 28, NULL, NULL),
(1, 29, NULL, NULL),
(2, 8, NULL, NULL),
(2, 27, NULL, NULL),
(2, 24, NULL, NULL),
(2, 3, NULL, NULL),
(2, 4, NULL, NULL),
(2, 15, NULL, NULL),
(2, 9, NULL, NULL),
(2, 10, NULL, NULL),
(2, 11, NULL, NULL),
(2, 12, NULL, NULL),
(2, 13, NULL, NULL),
(2, 14, NULL, NULL),
(2, 18, NULL, NULL),
(2, 17, NULL, NULL),
(2, 16, NULL, NULL),
(2, 19, NULL, NULL),
(2, 23, NULL, NULL),
(2, 20, NULL, NULL),
(2, 29, NULL, NULL),
(2, 26, NULL, NULL),
(2, 25, NULL, NULL),
(2, 2, NULL, NULL),
(1, 6, NULL, NULL),
(1, 7, NULL, NULL),
(1, 5, NULL, NULL),
(1, 3, NULL, NULL),
(1, 4, NULL, NULL),
(2, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(2, 3, NULL, NULL),
(2, 4, NULL, NULL),
(2, 6, NULL, NULL),
(2, 7, NULL, NULL),
(2, 8, NULL, NULL),
(2, 9, NULL, NULL),
(2, 10, NULL, NULL),
(3, 3, NULL, NULL),
(3, 9, NULL, NULL),
(3, 10, NULL, NULL),
(3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `first_name`, `last_name`, `email`, `telephone`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma', 'Administrator', 'iNat', 'Administrator', 'arangaw@gmail.com', '0718068183', 'images/bc185e9e82f314e7b33ce6d1227b16d7.jpg', 'njFxacnMaLBMAHHSBLS2lEdF9yHv5MvTom5MA6cerhZafA3BF7qoG8DsMgdJ', '2018-01-31 05:13:51', '2018-02-28 00:18:35'),
(2, 'radmin', '$2y$10$MBruwPhlXBD7jdbXAL9sBObd7NSBRzCJlsf.Q7DcFvIDtzvqdNkma', 'Reviva Administrator', 'Reviva', 'Administrator', 'admin@reviva.com', '0718068183', NULL, '3U9Uo7PLk08HvqDL28tBjlfkwrVaDVKcv8LNSgfn8PD7Gmq02VRVkqAR4Sn0', NULL, NULL),
(3, 'user', '$2y$10$ntu2S.WfkT2/ERYRpnuqo.Ijj9CMrKZj0OQUWDmKUiLay3fibIEaG', NULL, 'Test', 'User', 'test@admin.com', '0718068183', 'images/55f06bf6269d2740dd8fd23c6c5dfcc5.png', 'RBlVP0jHLUibE3AW0pTJK4GUkgFvAovWUBmvbds1iWRYWxHqiguAEvUmdjaP', '2018-02-21 16:26:07', '2018-02-28 03:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_user_permissions`
--

INSERT INTO `admin_user_permissions` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(3, 2, NULL, NULL),
(3, 3, NULL, NULL),
(3, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bincard`
--

CREATE TABLE `bincard` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `transaction_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` decimal(12,2) DEFAULT NULL,
  `debit` decimal(12,2) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bincard`
--

INSERT INTO `bincard` (`id`, `product_id`, `warehouse_id`, `transaction_description`, `credit`, `debit`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Desc', '10.00', '0.00', 'ACTIVE', '2018-02-19 05:19:08', '2018-02-19 05:19:08'),
(2, 1, 1, 'Desc', '10.00', '0.00', 'ACTIVE', '2018-02-19 05:47:14', '2018-02-19 05:47:14'),
(3, 2, 1, 'Desc', '100.00', '0.00', 'ACTIVE', '2018-02-19 05:47:15', '2018-02-19 05:47:15'),
(4, 1, 1, 'Desc', '100.00', '0.00', 'ACTIVE', '2018-02-20 03:03:51', '2018-02-20 03:03:51'),
(5, 2, 1, 'Desc', '50.00', '0.00', 'ACTIVE', '2018-02-20 03:03:51', '2018-02-20 03:03:51'),
(6, 1, 2, 'Stock Received - 2018-02-20', '10.00', '0.00', 'ACTIVE', '2018-02-20 04:20:26', '2018-02-20 04:20:33'),
(7, 1, 2, 'Stock Received - 2018-02-20', '10.00', '0.00', 'ACTIVE', '2018-02-20 04:33:43', '2018-02-20 04:33:43'),
(8, 1, 2, 'Stock Received - 2018-02-20', '10.00', '0.00', 'ACTIVE', '2018-02-20 05:09:42', '2018-02-20 05:10:17'),
(9, 2, 1, 'Desc 12', '0.00', '2.00', 'ACTIVE', '2018-02-20 05:12:25', '2018-02-20 05:12:25'),
(10, 2, 2, 'Desc 12', '2.00', '0.00', 'ACTIVE', '2018-02-20 05:12:33', '2018-02-20 05:12:33'),
(11, 1, 1, 'Desc12', '0.00', '20.00', 'ACTIVE', '2018-02-20 05:49:58', '2018-02-20 05:49:58'),
(12, 1, 2, 'Desc12', '20.00', '0.00', 'ACTIVE', '2018-02-20 05:49:59', '2018-02-20 05:49:59'),
(13, 1, 1, '', '0.00', '2.00', 'ACTIVE', '2018-02-20 05:51:45', '2018-02-20 05:51:45'),
(14, 1, 2, '', '2.00', '0.00', 'ACTIVE', '2018-02-20 05:51:45', '2018-02-20 05:51:45'),
(15, 1, 1, '', '0.00', '2.00', 'ACTIVE', '2018-02-20 05:52:09', '2018-02-20 05:52:09'),
(16, 1, 2, '', '2.00', '0.00', 'ACTIVE', '2018-02-20 05:52:09', '2018-02-20 05:52:09'),
(17, 1, 1, 'Test', '10.00', '0.00', 'ACTIVE', '2018-02-28 00:12:57', '2018-02-28 00:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Brand', 'ACTIVE', '2018-02-03 02:03:56', '2018-02-28 00:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Root', 'ACTIVE', NULL, '2018-02-03 02:29:02'),
(2, 1, 2, 'Main Category', 'ACTIVE', '2018-02-03 02:27:21', '2018-02-03 02:29:50'),
(3, 2, 3, 'Sub Category MC 1', 'INACTIVE', '2018-02-03 02:27:57', '2018-02-03 02:29:50'),
(4, 1, 4, 'Main Category 2', 'ACTIVE', '2018-02-03 02:28:45', '2018-02-03 02:29:50');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `default_price_list_id` int(11) NOT NULL,
  `default_tax_type_id` int(11) NOT NULL,
  `default_payment_term_id` int(11) NOT NULL,
  `default_payment_method_id` int(11) NOT NULL,
  `discount_rate` double NOT NULL,
  `minimum_order_value` double(12,2) NOT NULL,
  `address_line1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suburb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `logo`, `company_code`, `tax_number`, `phone_number`, `fax_number`, `website`, `email_address`, `description`, `default_price_list_id`, `default_tax_type_id`, `default_payment_term_id`, `default_payment_method_id`, `discount_rate`, `minimum_order_value`, `address_line1`, `address_line2`, `suburb`, `city`, `post_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Company 1', NULL, 'CC1', '1-1245-A123456', '0718068183', '0112456741', 'http://www.google.com', 'arangaw@gmail.com', 'Description', 1, 1, 1, 1, 0.25, 100000.00, 'Address 1', 'Address 2', 'Suburb', 'City', '12000', 'ACTIVE', '2018-02-02 04:21:02', '2018-02-02 04:21:02'),
(2, 'Company 2', 'images/Screenshot from 2017-12-23 23-18-45.png', 'CC2', '1-1245-A123456', '0718068183_', '0718068183_', 'http://www.google.com', 'arangaw@gmail.com', 'Descrption 2', 1, 1, 1, 1, 0.25, 25000.00, 'Address 1', 'Address 2', 'Suburb', 'City', '12000', 'ACTIVE', '2018-02-02 04:49:43', '2018-02-02 05:04:43');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double(10,2) NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `rate`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Srilankan Rupee', 'LKR', 'Rs.', 1.00, 'Yes', 'ACTIVE', '2018-01-31 05:15:00', '2018-01-31 05:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `grn`
--

CREATE TABLE `grn` (
  `id` int(10) UNSIGNED NOT NULL,
  `grn_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grn`
--

INSERT INTO `grn` (`id`, `grn_number`, `description`, `warehouse_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'GR-1', 'Desc', 1, 'ACTIVE', '2018-02-19 05:18:55', '2018-02-19 05:18:55'),
(2, 'GR-2', 'Desc', 1, 'ACTIVE', '2018-02-19 05:47:12', '2018-02-19 05:47:12'),
(3, 'GR-3', 'Desc', 1, 'ACTIVE', '2018-02-20 03:03:43', '2018-02-20 03:03:43'),
(4, 'GR4', 'Test', 1, 'ACTIVE', '2018-02-28 00:12:56', '2018-02-28 00:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `grn_items`
--

CREATE TABLE `grn_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `grn_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `qty` decimal(12,2) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grn_items`
--

INSERT INTO `grn_items` (`id`, `grn_id`, `product_id`, `warehouse_id`, `qty`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '10.00', 'ACTIVE', '2018-02-19 05:18:58', '2018-02-19 05:18:58'),
(2, 2, 1, 1, '10.00', 'ACTIVE', '2018-02-19 05:47:12', '2018-02-19 05:47:12'),
(3, 2, 2, 1, '100.00', 'ACTIVE', '2018-02-19 05:47:14', '2018-02-19 05:47:14'),
(4, 3, 1, 1, '100.00', 'ACTIVE', '2018-02-20 03:03:43', '2018-02-20 03:03:43'),
(5, 3, 2, 1, '50.00', 'ACTIVE', '2018-02-20 03:03:51', '2018-02-20 03:03:51'),
(6, 4, 1, 1, '10.00', 'ACTIVE', '2018-02-28 00:12:57', '2018-02-28 00:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_04_173148_create_admin_tables', 1),
(4, '2018_01_31_173148_create_config_tables', 1),
(5, '2018_02_02_173148_create_company_tables', 2),
(7, '2018_02_02_173148_create_warehouse_tables', 3),
(8, '2018_02_02_173148_create_product_related_tables', 4),
(16, '2018_02_13_173148_create_inventory_tables', 5),
(17, '2018_02_13_203148_create_grn_tables', 5),
(19, '2018_02_20_203148_create_stock_transfer_tables', 6),
(20, '2018_02_21_174831_alter_admin_user_table', 7),
(21, '2018_02_23_124631_alter_product_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cash on Delivery', 'No', 'ACTIVE', '2018-01-31 06:21:57', '2018-01-31 06:25:52');

-- --------------------------------------------------------

--
-- Table structure for table `payment_terms`
--

CREATE TABLE `payment_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_in_days` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_terms`
--

INSERT INTO `payment_terms` (`id`, `name`, `due_in_days`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cheque', '90', 'No', 'ACTIVE', '2018-01-31 06:37:28', '2018-01-31 06:37:58');

-- --------------------------------------------------------

--
-- Table structure for table `price_lists`
--

CREATE TABLE `price_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` int(11) NOT NULL,
  `rate` double(10,2) NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `price_lists`
--

INSERT INTO `price_lists` (`id`, `name`, `code`, `currency_id`, `rate`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Default Price List', 'DPL', 1, 1.00, 'No', 'ACTIVE', '2018-01-31 05:17:37', '2018-01-31 06:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `supplier_id` int(11) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `cost` decimal(12,2) DEFAULT NULL,
  `buying_price` decimal(12,2) DEFAULT NULL,
  `wholesale_price` decimal(12,2) DEFAULT NULL,
  `retail_price` decimal(12,2) DEFAULT NULL,
  `manage_stock_level` enum('YES','NO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_on_hand` decimal(12,2) DEFAULT NULL,
  `opening_stock` decimal(12,2) DEFAULT NULL,
  `re_order_level` decimal(12,2) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pictures` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `sku`, `description`, `supplier_id`, `product_type_id`, `brand_id`, `cost`, `buying_price`, `wholesale_price`, `retail_price`, `manage_stock_level`, `stock_on_hand`, `opening_stock`, `re_order_level`, `category_id`, `status`, `created_at`, `updated_at`, `pictures`) VALUES
(2, 'Product A', 'S', NULL, NULL, 1, NULL, NULL, NULL, NULL, '110.00', 'YES', NULL, NULL, '10.00', 2, 'ACTIVE', '2018-02-19 04:21:04', '2018-02-28 01:51:02', '["images\\/23949cfa51ecdc767430db870f6d9f96.jpg"]'),
(3, 'Product C', 'PC', 'Product C', 1, 1, NULL, '100.00', NULL, NULL, '125.00', 'YES', NULL, NULL, '150.00', NULL, 'ACTIVE', '2018-02-21 02:17:36', '2018-02-21 02:17:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Raw Meterial', 'ACTIVE', '2018-02-03 02:04:34', '2018-02-03 02:04:34'),
(2, 'Finished Goods', 'ACTIVE', '2018-02-05 02:56:35', '2018-02-05 02:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `mutual_balance` decimal(12,2) DEFAULT NULL,
  `actual_balance` decimal(12,2) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `product_id`, `warehouse_id`, `mutual_balance`, `actual_balance`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '56.00', '56.00', 'ACTIVE', '2018-02-19 05:19:02', '2018-02-28 00:12:57'),
(2, 2, 1, '148.00', '148.00', 'ACTIVE', '2018-02-19 05:47:15', '2018-02-20 05:12:20'),
(3, 1, 2, '2.00', '2.00', 'ACTIVE', '2018-02-20 05:52:05', '2018-02-20 05:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustment_reasons`
--

CREATE TABLE `stock_adjustment_reasons` (
  `id` int(10) UNSIGNED NOT NULL,
  `reason` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock_adjustment_reasons`
--

INSERT INTO `stock_adjustment_reasons` (`id`, `reason`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Reason 1', 'No', 'ACTIVE', '2018-01-31 06:30:40', '2018-01-31 06:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `stock_transfer`
--

CREATE TABLE `stock_transfer` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_warehouse_id` int(11) NOT NULL DEFAULT '0',
  `to_warehouse_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `qty` decimal(12,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock_transfer`
--

INSERT INTO `stock_transfer` (`id`, `from_warehouse_id`, `to_warehouse_id`, `product_id`, `qty`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, '10.00', 'Desc', 'ACTIVE', '2018-02-20 04:33:43', '2018-02-20 04:33:43'),
(2, 1, 2, 1, '10.00', 'Desc', 'ACTIVE', '2018-02-20 05:10:17', '2018-02-20 05:10:17'),
(3, 1, 2, 2, '2.00', 'Desc 12', 'ACTIVE', '2018-02-20 05:12:33', '2018-02-20 05:12:33'),
(4, 1, 2, 1, '20.00', 'Desc12', 'ACTIVE', '2018-02-20 05:49:59', '2018-02-20 05:49:59'),
(5, 1, 2, 1, '2.00', NULL, 'ACTIVE', '2018-02-20 05:51:45', '2018-02-20 05:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `city`, `post_code`, `phone_number`, `email_address`, `contact_person`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Supplier', 'Address', 'City', '12000', '0718068183', 'arangaw@gmail.com', 'Aranga Wijesooriya', 'ACTIVE', '2018-02-03 01:21:05', '2018-02-03 01:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `tax_types`
--

CREATE TABLE `tax_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double(10,2) NOT NULL,
  `is_default` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tax_types`
--

INSERT INTO `tax_types` (`id`, `name`, `code`, `rate`, `is_default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Vat', 'VAT', 15.00, 'No', 'ACTIVE', '2018-01-31 05:31:49', '2018-01-31 06:27:23'),
(2, 'NBT', 'NBT', 0.20, 'Yes', 'ACTIVE', '2018-01-31 05:33:26', '2018-01-31 05:33:26');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`id`, `name`, `address`, `city`, `post_code`, `phone_number`, `email_address`, `contact_person`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Main Warehouse', '"Kuladeepa",Thalgamuwa, Devanagala', 'City', '12000', '0718068183_', 'arangaw@gmail.com', 'Aranga Wijesooriya', 'ACTIVE', '2018-02-02 05:32:18', '2018-02-18 01:47:36'),
(2, 'Warehouse 1', 'Address 1, Address 2', 'Colombo', '12000', '718068183__', 'arangaw@gmail.com', 'Aranga Wijesooriya', 'ACTIVE', '2018-02-20 03:24:20', '2018-02-20 03:24:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `bincard`
--
ALTER TABLE `bincard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_type_unique` (`type`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_title_unique` (`title`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_company_name_unique` (`company_name`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currencies_name_unique` (`name`);

--
-- Indexes for table `grn`
--
ALTER TABLE `grn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grn_items`
--
ALTER TABLE `grn_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_methods_name_unique` (`name`);

--
-- Indexes for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_terms_name_unique` (`name`);

--
-- Indexes for table `price_lists`
--
ALTER TABLE `price_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `price_lists_name_unique` (`name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_types_type_unique` (`type`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_adjustment_reasons`
--
ALTER TABLE `stock_adjustment_reasons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stock_adjustment_reasons_reason_unique` (`reason`);

--
-- Indexes for table `stock_transfer`
--
ALTER TABLE `stock_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `suppliers_name_unique` (`name`);

--
-- Indexes for table `tax_types`
--
ALTER TABLE `tax_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_types_name_unique` (`name`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `warehouse_name_unique` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1611;
--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bincard`
--
ALTER TABLE `bincard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `grn`
--
ALTER TABLE `grn`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `grn_items`
--
ALTER TABLE `grn_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_terms`
--
ALTER TABLE `payment_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `price_lists`
--
ALTER TABLE `price_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stock_adjustment_reasons`
--
ALTER TABLE `stock_adjustment_reasons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stock_transfer`
--
ALTER TABLE `stock_transfer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tax_types`
--
ALTER TABLE `tax_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
